function WPN = makeFormula(WPN,OutputOptions)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Computes transmitted wavepacket using formula calculations, with option %
% to define the wavepacket at an initial point, and the option to evolve  %
% the wavepacket far from the crossing.                                   %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    [WPN,OutputOptions] = Preprocess(WPN,OutputOptions);
    [Wavepackets,Potentials,Numerics] = unpackWPN(WPN);
    direction = Numerics.formulaOptions.direction;
    
    formulaFn = str2func(Numerics.formula);

    
    if(Wavepackets.givenInitial)
        % Evolve wavepacket to crossing point and find formula
        disp('Evolving to crossing point and finding formula');
        Numerics.time = Numerics.tTotal;
        if(strcmp(direction,'down'))
            Potentials.V = 'makeVU';
        else
            Potentials.V = 'makeVL';
        end
        Wavepackets.PsiHat = Wavepackets.PsiHatUpInitial;
        Wavepackets.Psi = [];
        WPN = packWPN(Wavepackets,Potentials,Numerics);
        WavepacketsAC = DataStorage(OutputOptions.DataDirE,@DetectAvoidedCrossing,WPN,OutputOptions,false);
        Wavepackets.FormulaHatCrossing = WavepacketsAC.ACPsiHatFormula;
    else
        % Wavepacket given at crossing point.  
        disp('Applying Formula'); 
        Wavepackets.FormulaHatCrossing = formulaFn(WPN,OutputOptions);
    end
        
    WPN = packWPN(Wavepackets,Potentials,Numerics);
    
    % Evolve formula wavepacket on transmitted level away from avoided crossing
    if(~Numerics.compareAtCrossing)
        disp('Evolving back to crossing ...');
        if(strcmp(direction,'down'))
            Potentials.V = 'makeVL';
        else
            Potentials.V = 'makeVU';
        end
        Wavepackets.PsiHat = Wavepackets.FormulaHatCrossing;
        Wavepackets.Psi = [];
        Numerics.time = Numerics.tForward;

        WPN = packWPN(Wavepackets,Potentials,Numerics);

        WavepacketsF = DataStorage(OutputOptions.DataDirE,@ExactOneLevel,WPN,OutputOptions,false);
        Wavepackets.FormulaHatFinal = WavepacketsF.PsiHat;
    end
        
    %% Clean up
    Wavepackets.PsiHat = [];
    Wavepackets.Psi    = [];
        
    WPN = packWPN(Wavepackets,Potentials,Numerics);
    disp('Done.')

end

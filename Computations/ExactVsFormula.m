function [WPN,OutputOptions,RelErr,RelAbsErr] = ExactVsFormula(WPN,OutputOptions)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Computes transmitted wavepacket using the formula and exact calculation %
% and compares the results.                                               %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    [WPN,OutputOptions] = Preprocess(WPN,OutputOptions);    
    [Wavepackets,Potentials,Numerics] = unpackWPN(WPN);
    formulaFn = str2func(Numerics.formula);
    
    % Construct initial wavepacket
    disp('Computing initial wavepacket ...');
    [~,~,temp] = DataStorage(OutputOptions.DataDirE,@makePsiHatInitial,WPN,OutputOptions,false);
    PsiHatInitialFile = [temp.Filename '.mat'];
    disp('Done.');
    
    Numerics.time = Numerics.tTotal;
    

    % Find formula at crossing point
    if(Wavepackets.givenCrossing)
        % Wavepacket given at crossing point.  
        disp('Wavepacket given at crossing. Applying Formula'); 
        [~,~,temp] = DataStorage(OutputOptions.DataDirF,@FormulaAtCrossing,WPN,OutputOptions,false);
        disp('Done.');
    else
        % Wavepacket given at initial point
        disp('Detecting avoided crossing and applying formula...');
        Wavepackets.PsiHat = PsiHatInitialFile;
        Wavepackets.Psi    = [];

        if(strcmp(Numerics.formulaOptions.direction,'down'))
            Potentials.V = 'makeVU';
        else
            Potentials.V = 'makeVL';
        end

        WPN = packWPN(Wavepackets,Potentials,Numerics);

        [~,~,temp] = DataStorage(OutputOptions.DataDirF,@DetectAvoidedCrossing,WPN,OutputOptions,false);
        
    end
    Wavepackets.AC     = [temp.Filename '.mat'];
    Wavepackets.Psi    = [];
    Wavepackets.PsiHat = [];

    % Construct the potential operator
    disp('Computing potential operator ...');
    DataDirV = OutputOptions.DataDirV;
    PN.Potentials = Potentials;
    NumericsV = Numerics;
    NumericsV.tDir = sign(NumericsV.time);
    IgnoreList = {'tBackward','tForward','time','compareAtCrossing','direction', ...
                  'tTotal','tCrossing'};
    nIgnore = length(IgnoreList);
    for iIgnore = 1:nIgnore
        if(isfield(NumericsV,IgnoreList{iIgnore}))
            NumericsV = rmfield(NumericsV,IgnoreList{iIgnore});
        end
    end
    PN.Numerics = NumericsV;

    [~,~,temp] = DataStorage(DataDirV,@makeExpV,PN,OutputOptions,false);
    Numerics.ExpV = [temp.Filename '.mat'];
    disp('Done.');


    % Do exact calculation and formula result from crossing point
    disp('Doing exact calculation ...');

    Numerics.direction = Numerics.formulaOptions.direction;
    
    if(strcmp(Numerics.formulaOptions.direction,'down'))
        Potentials.VFormula = 'makeVL';
        Wavepackets.PsiHatUp = PsiHatInitialFile;
        Wavepackets.PsiHatDown = [];
    else
        Potentials.VFormula = 'makeVU';
        Wavepackets.PsiHatDown = PsiHatInitialFile;
        Wavepackets.PsiHatUp = [];
    end  
    
    WPN = packWPN(Wavepackets,Potentials,Numerics);
    Wavepackets = DataStorage(OutputOptions.DataDirE,@ExactTwoLevelWithFormula,WPN,OutputOptions,false);

    WPN = packWPN(Wavepackets,Potentials,Numerics);
    
    disp('Done.');
    
    % Print errors
    psiHatFormula = Wavepackets.PsiHatFormula;
    if(strcmp(Numerics.formulaOptions.direction,'down'))
        psiHatExact = Wavepackets.PsiHatDown;
    else
        psiHatExact = Wavepackets.PsiHatUp;
    end
    [RelErr,RelAbsErr] = makeError(psiHatFormula,psiHatExact)
    
    massFormula        = sqrt(sum(abs(psiHatFormula(:)).^2)* Numerics.pStep);
    
    massExact          = sqrt(sum(abs(psiHatExact(:)).^2)* Numerics.pStep);
    
    massError          = abs(massExact - massFormula)
                
    massRelError       = massError/massExact
end

function WPN = makeExact(WPN,OutputOptions)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Performs exact dynamics for a two level system, with the option define  %
% the wavepacket at the crossing, and evolve back to the crossing on      %
% adiabatic potential surfaces after the two level dynamics have taken    %
% place (Assumes the crossing point is at time 0).                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    [WPN,OutputOptions] = Preprocess(WPN,OutputOptions);
    [Wavepackets,Potentials,Numerics] = unpackWPN(WPN);
    
    direction = Numerics.formulaOptions.direction;
    
    %% Do one-level dynamics to compute initial condition
    disp('Computing initial wavepacket ...');
    [~,~,temp] = DataStorage(OutputOptions.DataDirE,@makePsiHatInitial,WPN,OutputOptions,false);
    PsiHatInitialFile = [temp.Filename '.mat'];
    disp('Done.');

    %% Do 2-level dynamics forward in time until far from crossing
    
    % Set initial conditions for two level calculations
    Numerics.time = Numerics.tTotal;

    % Construct the potential operator
    disp('Computing potential operator ...');
    DataDirV = OutputOptions.DataDirV;
    PN.Potentials = Potentials;
    NumericsV = Numerics;
    NumericsV.tDir = sign(NumericsV.time);
    IgnoreList = {'tBackward','tForward','time','compareAtCrossing','direction'};
    nIgnore = length(IgnoreList);
    for iIgnore = 1:nIgnore
        if(isfield(NumericsV,IgnoreList{iIgnore}))
            NumericsV = rmfield(NumericsV,IgnoreList{iIgnore});
        end
    end
    PN.Numerics = NumericsV;

    Numerics.ExpV = DataStorage(DataDirV,@makeExpV,PN,OutputOptions);
    disp('Done.');
    
    % Do two-level evolution
    disp('Computing exact 2-level dynamics ...');
    
    if(strcmp(direction,'down'))
        Wavepackets.PsiHatUp = PsiHatInitialFile;
        Wavepackets.PsiHatDown = zeros(size(Wavepackets.PsiHatUp));
    else
        Wavepackets.PsiHatDown = PsiHatInitialFile;
        Wavepackets.PsiHatUp = zeros(size(Wavepackets.PsiHatDown));
    end
        
    Wavepackets.PsiUp = []; 
    Wavepackets.PsiDown = [];
    
    WPN = packWPN(Wavepackets,Potentials,Numerics);
    Wavepackets = DataStorage(OutputOptions.DataDirE,@ExactTwoLevel,WPN,OutputOptions,false);

    Wavepackets.ExactHatUpFinal = Wavepackets.PsiHatUp;
    Wavepackets.ExactHatDownFinal = Wavepackets.PsiHatDown;

    %% Evolve back to crossing
    
    if(Numerics.compareAtCrossing)
        disp('Evolving back to crossing ...');
        Numerics.time = - Numerics.tForward;
        
        if(strcmp(direction,'down'))
            % Evolve lower level back
            Potentials.V = 'makeVL';
            Wavepackets.PsiHat = Wavepackets.ExactHatDownFinal;
            Wavepackets.Psi = [];
            WPN = packWPN(Wavepackets,Potentials,Numerics);
            Wavepackets = DataStorage(OutputOptions.DataDirE,@ExactOneLevel,WPN,OutputOptions,false);
            Wavepackets.ExactHatDownCrossing = Wavepackets.PsiHat;
            
        else  
            % Evolve upper level back
            Potentials.V = 'makeVU';
            Wavepackets.PsiHat = Wavepackets.ExactHatUpFinal;
            Wavepackets.Psi = [];
            WPN = packWPN(Wavepackets,Potentials,Numerics);
            Wavepackets = DataStorage(OutputOptions.DataDirE,@ExactOneLevel,WPN,OutputOptions,false);
            Wavepackets.ExactHatUpCrossing = Wavepackets.PsiHat;
        end
    end
    
    %% clean up
    Wavepackets.PsiUp = [];
    Wavepackets.PsiDown = [];
    Wavepackets.PsiHatUp = [];
    Wavepackets.PsiHatDown = [];
    Numerics.ExpV = [];

    WPN = packWPN(Wavepackets,Potentials,Numerics);
    disp('Done.');
end


function [eta,pCut,c1] = makeEta2DSlice(WPN,p,delta)
%% construct eta and cutoff for formula calculation
    [~,~,Numerics] = unpackWPN(WPN);

    direction = Numerics.formulaOptions.direction;
    if(strcmp(direction,'down'))
        etaSign = -1;
    else
        etaSign = +1;
    end
        
    % Create a logical statement, to make an indicator function
    % depending on which direction we're moving
    c1 = (p.^2 + etaSign*4*delta) > 0;
    signP = sign(p(c1));
    signP( p(c1) == 0 ) = 1; % fix case when p=0

    pCut = p(c1);
    
    eta = signP .* sqrt( p(c1).^2 + etaSign*4*delta );

end
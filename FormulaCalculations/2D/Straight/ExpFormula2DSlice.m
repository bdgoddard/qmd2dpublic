function [ExpFormula,tau,cmplxZero] = ExpFormula2DSlice(WPN,p1,delta,CoM)
%% Construct exponential prefactor for wavepacket
    [~,~,Numerics] = unpackWPN(WPN);

    epsilon = Numerics.epsilon;

    [tau,cmplxZero]  = makeTau2DSlice(WPN,CoM);
    [eta,pCut,pMask] = makeEta2DSlice(WPN,p1,delta);

    ExpFormula = zeros(size(p1));
    
    ExpFormula(pMask,1) = exp(-(imag(tau)/(2*delta*epsilon))*abs(pCut-eta))...
                   .*exp((-1i*real(tau))/(2*delta*epsilon)*(pCut-eta));

end


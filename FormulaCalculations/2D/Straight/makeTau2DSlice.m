function [tau,cmplxZero]=makeTau2DSlice(WPN,CoM)    

[~,Potentials,~] = unpackWPN(WPN);

% Find complex zero of rho
AbsRho = @(x)abs(makeRho(Potentials,x(1)+1i*x(2)+CoM(1),CoM(2)));
x=fminsearch(AbsRho,[0,0],optimset('TolX',1e-10));
cmplxZero=x(1)+1i*abs(x(2));  % ensure cmplxZero is in +ve half-plane


dt=0.0001;
tVals=(0:dt:1).'; % transpose to make a column vector
curveVals=tVals.*cmplxZero; % the curve to integrate over

% determine the value of rho at each point on the curve
rhoVals=makeRho(Potentials,curveVals+CoM(1),CoM(2)); 

% 2 int_0^cmplxZero rho = 2 cmplxZero int_0^1 rho(t cmplxZero)
tau=2*cmplxZero*sum(rhoVals)*dt;
end


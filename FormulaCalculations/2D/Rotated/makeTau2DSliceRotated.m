 function [tau,cmplxZero]=makeTau2DSliceRotated(WPN,theta,CoM)    
%% Construct tau for rotated wavepacket.
[~,Potentials,~] = unpackWPN(WPN);

% find complex zero of slice of rho
AbsRho = @(x)abs(makeRho( Potentials,cos(theta)*(x(1)+1i*x(2)) + CoM(1),sin(theta)*(x(1)+1i*x(2)) + CoM(2) ));
x=fminsearch(AbsRho,[0,0],optimset('TolX',1e-16));
cmplxZero=x(1)+1i*abs(x(2)); % ensure cmplxZero is in +ve half-plane

dt=0.0001;
tVals=(0:dt:1).'; % transpose to make a column vector
curveVals=tVals.*cmplxZero; % the curve to integrate over

% determine the value of rho at each point on the curve
rhoVals = makeRho( Potentials,cos(theta)*(curveVals)+CoM(1),sin(theta)*(curveVals)+CoM(2) );

% 2 int_x0^cmplxZero rho = 2 (cmplxZero-x0) int_0^1 rho(t (cmplxZero+x0))
tau=2*cmplxZero*sum(rhoVals)*dt;
end


function formulaHat=Formula2DSlicingRotated(WPN,OutputOps)
% Applies the slicing algorithm using 1D formula to a 2D wavepacket which
% is not travelling in x direction

%% Initialisation
[Wavepackets,Potentials,Numerics] = unpackWPN(WPN);

[x,~,xg,pg] = makeXP(Numerics);
Waitbar   = OutputOps.Waitbar;

direction = Numerics.formulaOptions.direction;
d         = Numerics.d;
epsilon   = Numerics.epsilon;

ExpFn = str2func(Numerics.formulaOptions.ExpFunction); 
PreFn = str2func(Numerics.formulaOptions.PrefactorFunction); 

if(strcmp(direction,'down'))
    etaSign = -1;
else
    etaSign = +1;
end

if Waitbar
    wbe = waitbar(0,'Rotating coordinates in direction of momentum');
end

% Construct wavepacket at crossing
if(strcmp(direction,'down'))
    psiHatFn = Wavepackets.PsiHatUpCrossing;
else
    psiHatFn = Wavepackets.PsiHatDownCrossing;
end
if(isa(psiHatFn,'char'))
    psiHat = makePsiHatFull(psiHatFn,Wavepackets,Numerics);
else
    psiHat = psiHatFn;
end

%% Interpolate wavepacket on to a rotated grid
% Find centre of momentum of wavepacket
Numerics.xg = xg;
Numerics.pg = pg;
psi         = eIFTn(psiHat,Numerics);


if isfield(Wavepackets,'CoMMom')
    CoMom = Wavepackets.CoMMom;
else
    CoMom = makeCoM(psiHat,pg,[Numerics.pStep,Numerics.pStep],d);
end

if isfield(Wavepackets,'CoM')
    CoM = Wavepackets.CoM;
else
    CoM = makeCoM(psi,xg,[Numerics.xStep,Numerics.xStep],d);
end

% Find angle of direction of wavepacket
% NOTE: angle is between -pi and pi, with [0,1] having angle zero, [0,-1]
% having angle pi or -pi.
u = [1 0];
v = CoMom;
theta = sign(v(2)) * acos((u(1)*v(1)+u(2)*v(2))/(sqrt(u(1).^2+u(2).^2)*sqrt(v(1).^2+v(2).^2)));

% Remove phase before rotating
phaseOriginal = exp (1i/epsilon * ( CoMom(1)*(xg(:,:,1) - CoM(1)) + CoMom(2)*(xg(:,:,2) - CoM(2)) ) );
psi = psi ./ phaseOriginal;

% Rotate wavepacket
psi = RotateFunction(psi,x,theta,CoM);
xgRot = RotateGrid2D(xg,theta,CoM);

% Put phase back
phaseRotated = exp (1i/epsilon * ( CoMom(1)*(xgRot(:,:,1) - CoM(1)) + CoMom(2)*(xgRot(:,:,2) - CoM(2)) ) );
psi = psi .* phaseRotated;

%% Initialize 1D formula parameters
% Determine cutoff
maxPsi    = max( abs(psi(:)) );
relCutoff = Numerics.formulaOptions.relativeCutoff * maxPsi;

% Initialize the formula
formula = zeros(size(psi));

if Waitbar
    waitbar(0,wbe,'Applying the slicing algorithm');
end

% Numerics for 1D formula application
Numerics1D.d         = 1;
Numerics1D.xStep     = Numerics.xStep;
Numerics1D.NumPoints = Numerics.NumPoints;
Numerics1D.epsilon   = Numerics.epsilon;

Numerics1D.xStart    = x(1,1);
Numerics1D.xEnd      = x(end,1);
Numerics1D.pStep     = Numerics.pStep;

[x1,p1,xg1D,pg1D] = makeXP(Numerics1D);
Numerics1D.xg     = xg1D;
Numerics1D.pg     = pg1D;

signP          = sign(p1);
signP( p1==0 ) = 1; % fix case when p=0

NumPoints = Numerics1D.NumPoints;
%% Loop through slices of wavepacket
m=1;
for n=1:NumPoints
    if Waitbar
        waitbar( n/NumPoints, wbe, 'Applying the slicing algorithm');
    end
    
    % take an x2 slice
    psiupSlice = psi(:,n);

    x1Rot = xgRot(:,n,1);
    x2Rot = xgRot(:,n,2);    
       
    % Cutoff to make computation quicker 
    if( max(abs(psiupSlice)>relCutoff) )
  
        % Find Fourier transform of slice in 1D
        psiSliceHat = eFTn(psiupSlice,Numerics1D);

        % find CoM of slice in 2D
        CoMx = makeCoM(psiupSlice,x1Rot,x1Rot(2)-x1Rot(1),1);
        if x2Rot(2)-x2Rot(1)==0
            CoMy = x2Rot(1);
        else
            CoMy = makeCoM(psiupSlice,x2Rot,x2Rot(2)-x2Rot(1),1);
        end
                
        % Find delta on the slice
        delta = min(makeRho(Potentials,x1Rot,x2Rot));
        deltan(m) = delta;
        % Find eta and the closest corresponding point in p1
        eta    = signP.*sqrt(p1.^2 + etaSign*4*delta);
        etaPos = ((1:NumPoints).'+round(real(eta-p1)/Numerics1D.pStep));
        
        % Create momentum cutoff mask
        pMask = (p1.^2 + etaSign*4*delta)>0;
      
        % Determine wavepacket in eta
        etaPos     = etaPos(pMask); 
        psi0HatEta = zeros(size(p1));
        psi0HatEta(pMask) = psiSliceHat(etaPos);
        
        % Get exponential (LZ-like) factor
        [ExpFactor,tau(m),~] = ExpFn(WPN,p1,delta,theta,[CoMx CoMy]);        
        
        % Get prefactor
        Prefactor = PreFn(WPN,p1,delta);
        
        % CoM factor from derivation of kappa
        CoM1D     = makeCoM(psiupSlice,x1,x1(2)-x1(1),1);
        CoM1Dn(m) = CoM1D;
        CoMFactor = zeros(size(p1));
        CoMFactor(pMask) = exp(-1i* ((p1(pMask)-p1(etaPos)) * CoM1D)/epsilon);
        CoMFactor=1;
        % Apply the formula to psi0HatEta
        formulaSliceHat = CoMFactor .* Prefactor .* ExpFactor .* psi0HatEta; 
        
        % Add slice into transmitted spatial wavepacket
        formulaSlice = eIFTn(formulaSliceHat,Numerics1D);        
        formula(:,n) = formulaSlice; 
        xTest(m) = x1(n);
        m=m+1;

    end
   
end

% CoMom has shifted due to formula, removing old phase will be enough for
% interpolation
formula = formula ./ phaseRotated;

if theta~=0
    % Interpolate result on to original grid
    x = xgRot(:,:,1); 
    y = xgRot(:,:,2);
    F = scatteredInterpolant(x(:),y(:),formula(:),'natural','none');

    formula = F(xg(:,:,1),xg(:,:,2));
    formula(isnan(formula)) = 0;
end

% Replace phase
formula = formula .* phaseOriginal;

formulaHat = eFTn(formula,Numerics);

if Waitbar
	close(wbe)
end
    
    

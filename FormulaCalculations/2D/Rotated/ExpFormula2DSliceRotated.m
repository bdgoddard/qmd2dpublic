function [ExpFormula,tau,cmplxZero] = ExpFormula2DSliceRotated(WPN,p1,delta,theta,x0)
%% Construct exponential prefactor for rotated wavepacket

    epsilon = WPN.Numerics.epsilon;

    [tau,cmplxZero]  = makeTau2DSliceRotated(WPN,theta,x0);
    [eta,pCut,pMask] = makeEta2DSlice(WPN,p1,delta);

    ExpFormula          = zeros(size(p1));
    ExpFormula(pMask,1) = exp(-(imag(tau)/(2*delta*epsilon))*abs(pCut-eta))...
                   .*exp((-1i*real(tau))/(2*delta*epsilon)*(pCut-eta));
               
end


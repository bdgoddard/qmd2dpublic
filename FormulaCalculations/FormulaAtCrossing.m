function WavepacketsAC = FormulaAtCrossing(WPN,OutputOptions)

% Initialize
[Wavepackets,~,Numerics] = unpackWPN(WPN);
formulaFn = str2func(Numerics.formula);


WavepacketsAC = struct;

if(strcmp(Numerics.formulaOptions.direction,'down'))
    WavepacketsAC.ACPsiHat        = Wavepackets.PsiHatUpCrossing;
else
    WavepacketsAC.ACPsiHat        = Wavepackets.PsiHatDownCrossing;
end

WavepacketsAC.noAC            = 1;
WavepacketsAC.ACPsiHatFormula = formulaFn(WPN,OutputOptions);
WavepacketsAC.ACTimePos = round(abs(Numerics.tBackward)/Numerics.timestep);

end
function tau=makeTau1D(WPN)    
% Construct tau for 1D formula calculation

[Wavepackets,Potentials,~] = unpackWPN(WPN);

if(isfield(Wavepackets,'CoM'))
    CoM = Wavepackets.CoM;
else
    CoM = 0;
end


AbsRho = @(x)abs(makeRho(Potentials,x(1) + CoM +1i*x(2)));
x=fminsearch(AbsRho,[0,0],optimset('TolX',1e-10));
cmplxZero= x(1) + CoM + 1i * abs(x(2));
dt=0.0001;
tVals=(0:dt:1).'; % transpose to make a column vector

curveVals=CoM + tVals.*(cmplxZero-CoM); % the curve to integrate over

% determine the value of rho at each point on the curve
rhoVals=makeRho(Potentials,curveVals); 

% 2 int_0^cmplxZero rho = 2 cmplxZero int_0^1 rho(t cmplxZero)
tau=2*(cmplxZero-CoM)*sum(rhoVals)*dt;
end


function formulaHat = Formula1D(WPN,OutputOptions)
% Applies the formula to a 1D wavepacket.

[Wavepackets,~,Numerics] = unpackWPN(WPN);

direction = Numerics.formulaOptions.direction;
NumPoints = 1:Numerics.NumPoints;
pStep = Numerics.pStep;

ExpFn = str2func(Numerics.formulaOptions.ExpFunction); 
PreFn = str2func(Numerics.formulaOptions.PrefactorFunction); 

if(strcmp(direction,'down'))
    psiHat = Wavepackets.PsiHatUpCrossing;
else
    psiHat = Wavepackets.PsiHatDownCrossing;
end

% Get wavepacket at the avoided crossing in eta
[~,p,~,~] = makeXP(Numerics);
psi0HatEta = zeros(size(p));
[eta,~,pMask] = makeEta(WPN,p);

if(isa(psiHat,'char'))
    % Wavepacket is given by a function (handle as a string)
    psihat = str2func(psiHat);
    psi0HatEta(pMask,1) = psihat(Wavepackets,eta);     
else      
    % Wavepacket is given numerically on p grid
    [~,etaPos] = min( abs(bsxfun(@minus,eta.',p)));

    psi0HatEta(pMask,1) = psiHat(etaPos);
end

% Get exponential (LZ-like) factor
ExpFactor = ExpFn(WPN,p);

% Get prefactor
Prefactor = PreFn(WPN,p);

% Centre of mass prefactor
% Comes from shifting spatial argument of \kappa, which introduces a phase
% shift in \hat\kappa(k-\eta)
if(isfield(Wavepackets,'CoM'))
    CoM = Wavepackets.CoM;
    CoMFactor = zeros(size(p));
    epsilon = Numerics.epsilon;
    [~,etaPos] = min( abs(bsxfun(@minus,eta.',p)));
    CoMFactor(pMask) = exp(-1i* (p(pMask)-p(etaPos)) * CoM /epsilon);
else
    CoMFactor = 1;
end

formulaHat = CoMFactor.*Prefactor.* ExpFactor.* psi0HatEta;

end
function [ExpFormula,tau] = ExpFormula1D(WPN,p)    
% Construct exponential prefactor for formula calculation.
    [Wavepackets,Potentials,Numerics] = unpackWPN(WPN);

    if(isfield(Wavepackets,'CoM'))
        CoM = Wavepackets.CoM;
    else
        CoM = 0;
    end
    
    epsilon = Numerics.epsilon;

    delta = makeRho(Potentials,CoM);
    tau   = makeTau1D(WPN);
        
    [eta,pCut,pMask] = makeEta(WPN,p);

    ExpFormula = zeros(size(p));
    
    ExpFormula(pMask,1) = exp(-(imag(tau)/(2*delta*epsilon))*abs(pCut-eta))...
                   .*exp((-1i*real(tau))/(2*delta*epsilon)*(pCut-eta));

end


function Prefactor = PrefactorFormula1D(WPN,p)

    % Get eta and allowed p
    [eta,pCut,pMask] = makeEta(WPN,p);

    Prefactor = zeros(size(p));
    Prefactor(pMask,1) = ((pCut+eta)./(2*abs(eta)));

end
function [eta,pCut,c1] = makeEta(WPN,p)
% Construct eta for the formula calculation
    [Wavepackets,Potentials,Numerics] = unpackWPN(WPN);

    if(isfield(Numerics,'formulaOptions'))
        direction = Numerics.formulaOptions.direction;
    else
        direction = Numerics.direction;
    end
    
    if(strcmp(direction,'down'))
        etaSign = -1;
    else
        etaSign = +1;
    end
    
    if(isfield(Wavepackets,'CoM'))
        CoM = Wavepackets.CoM;
    else
        CoM = 0;
    end
    
    delta = makeRho(Potentials,CoM);
    
    % Create a logical statement, to make an indicator function
    % depending on which direction we're moving
    c1 = (p.^2 + etaSign*4*delta) > 0;
    signP = sign(p(c1));
    signP( p(c1) == 0 ) = 1; % fix case when p=0

    pCut = p(c1);
    
    eta = signP .* sqrt( p(c1).^2 + etaSign*4*delta );

end
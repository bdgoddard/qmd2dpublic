function Wavepackets=ExactOneLevel(WPN,OutputOptions)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculate the evolution of a wavepacket on a nD Potential V. Output is  %
% the wavepacket in position and momentum space, and optional checks.     %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[Wavepackets,Potentials,Numerics] = unpackWPN(WPN);

[x,p,xg,pg] = makeXP(Numerics);
Numerics.x = x;
Numerics.p = p;
Numerics.xg = xg;
Numerics.pg = pg;

time = Numerics.time; 
dt = Numerics.timestep;
d  = Numerics.d; 
dx = Numerics.xStep;
dp = Numerics.pStep;

if(d==2)
    x1 = x(:,1);
    x2 = x(:,2);
end

% Potentials
repGrid = replicateGrid(xg,Numerics);
Vfunc = str2func(Potentials.V);

V=Vfunc(Potentials,repGrid{:});
epsilon=Numerics.epsilon;

% Output options
Waitbar           = OutputOptions.Waitbar;
CheckEnergy       = OutputOptions.CheckTotalEnergy; 
CheckMass         = OutputOptions.CheckTotalMass;
CheckMomentum     = OutputOptions.CheckTotalMomentum;
CentreofMass      = OutputOptions.CheckCOM;
CentreofMomentum  = OutputOptions.CheckCOMom;
PlotAnimation     = OutputOptions.PlotAnimation;
SaveAnimation     = OutputOptions.SaveAnimation;
DataFolder        = OutputOptions.DataDirO;

% Initial conditions
if (isfield(Wavepackets,'Psi') && ~isempty(Wavepackets.Psi) )
    if(ischar(Wavepackets.Psi))
        psi = makePsiFull(Wavepackets.Psi,Wavepackets,Numerics);
    else
        psi = Wavepackets.Psi;
    end
    psiHat=eFTn(psi,Numerics);
else
    if(ischar(Wavepackets.PsiHat))
        psiHat = makePsiHatFull(Wavepackets.PsiHat,Wavepackets,Numerics);
    else
        psiHat = Wavepackets.PsiHat;
    end
    psi = eIFTn(psiHat,Numerics);
end

boundPsi = 2*max(abs(psi(:)));

% determine whether we're evolving forwards or backwards
% tDir is the sign change for the forwards in time eFT
if(time<0)
    tDir=-1;
    message={'Backwards Evolution on V'};
else
    tDir=1;
    message={'Forwards Evolution on V'};
end

% KE and V evolution operators
kdiag1 = exp(-1i*tDir*dt*(sum(pg.^2,d+1))/(2*epsilon)/2); % half a timestep
kdiag2 = exp(-1i*tDir*dt*(sum(pg.^2,d+1))/(2*epsilon)); % full timestep

expV = exp(-1i*tDir*dt*V/epsilon); % full timestep

% get number of steps
steps = round(abs(time)/dt);

%%%%%%%%%%%%%%%%%%%%%          Checks          %%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Initialize the energy, mass and centre of mass checks
if CheckEnergy == 1
    energy = zeros(steps,1);
end
if CheckMass == 1
    mass = zeros(steps,1);
end
if CheckMass == 1
    momentum = zeros(steps,1);
end
if CentreofMass == 1
    CoM = zeros(steps,d);
end
if CentreofMomentum == 1
    CoMom = zeros(steps,d);
end

% Begin the waitbar    
if Waitbar == 1
    wbe = waitbar(0,message{:});
end

%% Perform the dynamics
% Enable a faster Strang splitting by doing a half timestep first, then
% whole timesteps, until the final step, where we do half again.

% do initial half timestep for KE
psiHat = kdiag1.*psiHat;

% transform to position space
psi = eIFTn(psiHat,Numerics);

%%%%%%%%%%%%%%%%%          Saving Animation          %%%%%%%%%%%%%%%%%%%%%%
if SaveAnimation == 1
    filename = [DataFolder filesep  getTimeStr() '_ExactOneLevel'];
    
    % create AVI object
    vidObj = VideoWriter([ filename '.avi']);
    vidObj.Quality = 100;
    vidObj.FrameRate = 60;
    open(vidObj);
    
    axLims = [-15 15 -15 15];
end

for t = 1:steps
    % Advance waitbar
    if Waitbar == 1
        waitbar( abs(t)/steps, wbe, message{:});
    end  
    
    % full timestep V
    psi = expV.*psi;
    
    % transform to momentum space
    psiHat = eFTn(psi,Numerics);  
    if (t<steps)
        % full timestep KE#
        psiHat = kdiag2.*psiHat;
    else
        % half timestep KE
        psiHat = kdiag1.*psiHat;
    end
  
    % transform to position space
    psi = eIFTn(psiHat,Numerics);
    
%%%%%%%%%%%%%%%%%%%%%           Checks           %%%%%%%%%%%%%%%%%%%%%%%%%%
    if CheckEnergy == 1
    % Check the energy of the system at time t
        Z1        = (abs(psi).^2).*V; % PE
        Z2        = (abs(psiHat).^2).*((sum(pg.^2,d+1))/2); % KE
        energy(t) = dx^d*sum(Z1(:))+dp^d*sum(Z2(:)); % Total
    end
    if CheckMass == 1
        mass(t) = sqrt(dx^d*sum(abs(psi(:)).^2));
    end
    if CheckMomentum == 1
        momentum(t) = sqrt(dp^d*sum(abs(psiHat(:)).^2));
    end

    if CentreofMass == 1
        M = dx^d*sum(abs(psi(:)).^2);
        for dim = 1:d
            integrand  = abs(psi).^2.*repGrid{dim};
            CoM(t,dim) = 1/M.*dx^d*sum(integrand(:));
        end
    end
    
    if CentreofMomentum == 1
        M = dp^d*sum(abs(psiHat(:)).^2);
        for dim = 1:d
            integrand  = abs(psiHat).^2.*repGrid{dim};
            CoMom(t,dim) = 1/M.*dp^d*sum(integrand(:));
        end
    end
    
%%%%%%%%%%%%          Plotting and Saving Animation          %%%%%%%%%%%%%%
    if PlotAnimation==1
        if d==2
            % Note: contourm has the same problem as meshgrid! Hence
            % transposes
            [xgM,ygM,psiM] = maskPsi(psi,x,dx,10^(-3));
            if t==1
                % Bounds for colorbar
                Vmin = min(V(:));
                Vmax = max(V(:));

                %create figure and store handle
                hF = figure('units','normalized','outerposition',[0 0 1 1]);
                set(hF,'color','w')
                axis off
                title('One-Level calculation')

                % VU axes and plot
                hAxesV = axes('Position',[0.1,0.25,0.65,0.65]);
                hAxesV.FontSize = 7;
                VPlot1 = contourm(xg(:,:,1),xg(:,:,2),V.',20);
                axis(axLims)
                xlabel('x')
                ylabel('y')

                title('V','FontSize',12)

                colormap(hAxesV,hot);
                caxis(hAxesV,[Vmin Vmax]);

                % V colorbar for potential plots
                cbP = colorbar(hAxesV,'Position',[0.8, 0.3 , 0.05, 0.55]);
            else
                figure(hF)
                % Delete psiU and psiD axes
                delete(hAxesWP)
            end
            % psiU axes
            hAxesWP = axes('Position',[0.1,0.25,0.65,0.65]);
            axis(hAxesWP,'off') %set visibility for axes to 'off'
            hAxesWP.FontSize = 7;
            colormap(hAxesWP,cool); %set colormap
            axis(axLims)            
            WPPlot1 = contourm(xgM,ygM,abs(psiM).',10);

            % psiU colorbar
            cbpsiU = colorbar(hAxesWP,'Location','SouthOutside');
            set(cbpsiU, 'Position',[0.1,0.1,0.65,0.05]);
            title(cbpsiU,'\psi^U','FontSize',12)
            caxis(hAxesWP,[0 boundPsi]);

            % link the two overlaying axes so they match at all times to remain accurate
            linkaxes([hAxesV,hAxesWP]);
        elseif d==1
            plot(x,abs(psi))
            drawnow
        else
            message('I cannot plot in higher dimensions');
        end
        if SaveAnimation==1
            writeVideo(vidObj, getframe(hF));
        end
        
    end
    
end
if Waitbar==1
    close(wbe);
end

Wavepackets.Psi=psi;
Wavepackets.PsiHat=psiHat;

%%%%%%%%%%%%%%%%%%%%%%%%%       Checks      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Save energy checks etc in Wavepacket struct
if CheckEnergy==1
    Wavepackets.EnergyOneLevel=energy;
end
if CheckMass==1
    Wavepackets.MassOneLevel=mass;
end
if CheckMomentum==1
    Wavepackets.MomentumOneLevel=momentum;
end
if CentreofMass==1
    CoMSep=mat2cell(CoM,steps,ones(d,1));
    Wavepackets.CoMOneLevel=[CoM,Vfunc(Potentials,CoMSep{:})];
end
if CentreofMomentum==1
    CoMomSep=mat2cell(CoMom,steps,ones(d,1));
    Wavepackets.CoMomOneLevel=[CoMom,Vfunc(Potentials,CoMomSep{:})];
end

if SaveAnimation == 1
    close(vidObj);
    disp(['Animation saved in' DataFolder filesep  getTimeStr() '_ExactOneLevel'])
end

end
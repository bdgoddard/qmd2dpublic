function  Wavepackets = ExactTwoLevelWithFormula(WPN,OutputOptions)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculate the evolution of the wavepacket on a 2D potential V.  Output  %
% is two vectors in momentum space.  Also compute the instantaneous       %
% introduction and propagation of formula results for an  avoided         %
% crossings.                                                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% Output options
Waitbar          = OutputOptions.Waitbar;
CheckEnergy      = OutputOptions.CheckTotalEnergy; 
CheckMass        = OutputOptions.CheckTotalMass;
CheckMomentum    = OutputOptions.CheckTotalMomentum;
CentreofMass     = OutputOptions.CheckCOM;
CentreofMomentum = OutputOptions.CheckCOMom;

PlotAnimation = OutputOptions.PlotAnimation; 
SaveAnimation = OutputOptions.SaveAnimation;
DataFolder    = OutputOptions.DataDirO;

[Wavepackets,Potentials,Numerics] = unpackWPN(WPN);

%% Initialisation
% Unload Numericsfilename
time = Numerics.time; 
dt   = Numerics.timestep;

[x,p,xg,pg] = makeXP(Numerics);
Numerics.x  = x;
Numerics.p  = p;
Numerics.xg = xg;
Numerics.pg = pg;

NumPts    = Numerics.NumPoints;
RepNumPts = num2cell(size(pg));

d         = Numerics.d; 
dx        = Numerics.xStep; 
dp        = Numerics.pStep;
epsilon   = Numerics.epsilon;
direction = Numerics.direction;


% Construct diabatic Potentials
repGrid     = replicateGrid(xg,Numerics);
[V1,V2,V12] = makeV1V2V12(Potentials,repGrid{:});

% Potential for formula
VFFn = str2func(Potentials.VFormula);
VF   = VFFn(Potentials,repGrid{:});

% Load avoided crossing data
if(ischar(Wavepackets.AC))
    temp   = load(Wavepackets.AC);
    AC     = temp.Data;
else
    AC = Wavepackets.AC;
end

ACPsiHatFormula = AC.ACPsiHatFormula;
ACTimesPos      = AC.ACTimePos;
noAC            = 1;

if ( ( isfield(Wavepackets,'PsiUp') && ~isempty(Wavepackets.PsiUp) ) ...
        || ( isfield(Wavepackets,'PsiDown') && ~isempty(Wavepackets.PsiDown) ) )
    initialSpace = true;
else
    initialSpace = false;
end

% Initial wavepacket
if initialSpace % have to define PsiInitial or PsiHatInitial for one level dynamics
    
    if(ischar(Wavepackets.PsiUp))
        psiUp = makePsiFull(Wavepackets.PsiUp,Wavepackets,Numerics);
    else
        psiUp = Wavepackets.PsiUp;
    end
    
    if(ischar(Wavepackets.PsiDown))
        psiDown = makePsiFull(Wavepackets.PsiDown,Wavepackets,Numerics);
    else
        psiDown = Wavepackets.PsiDown;
    end

    psiHatUp   = eFTn(psiUp,Numerics);
    psiHatDown = eFTn(psiDown,Numerics);
else

    if(ischar(Wavepackets.PsiHatUp))
        psiHatUp = makePsiHatFull(Wavepackets.PsiHatUp,Wavepackets,Numerics);
    else
        if(~isempty(Wavepackets.PsiHatUp))
            psiHatUp = Wavepackets.PsiHatUp;
        else
            psiHatUp = zeros(RepNumPts{1:d},1);
        end
    end
   
    if(ischar(Wavepackets.PsiHatDown))
        psiHatDown = makePsiHatFull(Wavepackets.PsiHatDown,Wavepackets,Numerics);        
    else
        if(~isempty(Wavepackets.PsiHatDown))
            psiHatDown = Wavepackets.PsiHatDown;
        else
            psiHatDown = zeros(RepNumPts{1:d},1);
        end

    end
    psiUp      = eIFTn(psiHatUp,Numerics);
    psiDown    = eIFTn(psiHatDown,Numerics);
end

% Plotting bounds
if PlotAnimation
    
    absFormula = 0;
    
    for iAC = 1:noAC
        absFormula = absFormula + max(abs(ACPsiHatFormula(:)));
    end
    
    if(strcmp(direction,'down'))
        boundU = 2 * max(abs(psiUp(:)));
        boundD = absFormula;
    else
        boundD = 2 * max(abs(psiDown(:)));
        boundU = absFormula;
    end
end


%% Construct the KE and PE operators
% determine whether we're evolving forwards or backwards
% tDir is the sign change for the forwards in time eFT
if ( time<0 )
    tDir    = -1;
    message = {'Backwards Evolution on V'};
else
    tDir    = 1;
    message = {'Forwards Evolution on V'};
end

% KE operators in p-space, half and full time step for Strang Splitting

kdiag1 = exp(-1i*tDir*dt*(sum(pg.^2,d+1))/(2*epsilon)/2); % half a timestep
kdiag2 = exp(-1i*tDir*dt*(sum(pg.^2,d+1))/(2*epsilon)); % full timestep
expVF  = exp(-1i*tDir*dt*VF/epsilon); % full timestep

[Rho,X,Z,Tr] = makeRhoXZTrace(Potentials,repGrid{:});

if( ischar(Numerics.ExpV) )
    temp = load(Numerics.ExpV);
    u    = temp.Data;
else
    u    = Numerics.ExpV;
end

if d == 2    
    % separate u into 4 m x n matrices to avoid a for loop in the dynamics
    aV  = squeeze(u(1,1,:,:));
    bV  = squeeze(u(1,2,:,:));
    cV  = squeeze(u(2,1,:,:));
    dV = squeeze(u(2,2,:,:));
elseif d == 1
    % separate u into 4 m x n matrices to avoid a for loop in the dynamics
    aV  = squeeze(u(1,1,:));
    bV  = squeeze(u(1,2,:));
	cV  = squeeze(u(2,1,:));
    dV = squeeze(u(2,2,:));
else
    disp('Higher Dimensions not yet supported')
end

%% Move to the Diabatic regime using eigenfunctions

% deal with the case when X is close to zero
mask    = abs(X)<10^(-15);
maskPos = mask & (Z>=0);
maskNeg = mask & (Z<0);

% upper level
Phi1plus  = (Z+sqrt(Z.^2+X.^2))./X;
% (Z + abs(Z))/X + O(X)
Phi1minus = ones(size(Phi1plus));

% deal with region where X is small
Phi1plus(maskPos)  = 1;
Phi1minus(maskPos) = 0;
Phi1plus(maskNeg)  = 0;
Phi1minus(maskNeg) = 1;

% normalization
k1        = 1./sqrt(Phi1plus.^2+Phi1minus.^2);
Phi1plus  = k1.*Phi1plus;
Phi1minus = k1.*Phi1minus;

% lower level
Phi2plus  = (Z-sqrt(Z.^2+X.^2))./X;
% (Z - abs(Z))/X + O(X)
Phi2minus = ones(size(Phi2plus));

% deal with region where X is small
Phi2plus(maskPos)  = 0;
Phi2minus(maskPos) = 1;
Phi2plus(maskNeg)  = -1;
Phi2minus(maskNeg) = 0;

% normalization
k2        = 1./sqrt(Phi2plus.^2+Phi2minus.^2);
Phi2plus  = k2.*Phi2plus;
Phi2minus = k2.*Phi2minus;

% The diabatic wavefunctions
if(strcmp(direction,'down'))
    psiPlus  = psiUp.*Phi1plus;
    psiMinus = psiUp.*Phi1minus; % Assumes there is nothing on lower level.
else
    psiPlus  = psiDown.*Phi2plus;
    psiMinus = psiDown.*Phi2minus; % Assumes there is nothing on upper level.
end
    
psiHatPlus    = eFTn(psiPlus,Numerics);
psiHatMinus   = eFTn(psiMinus,Numerics);
psiHatFormula = zeros(size(psiHatPlus));

% Get number of time steps
steps = round(abs(time)/dt);
  
%% Initialize Checks
if CheckEnergy
    energyup   = zeros(1,steps);
    energydown = zeros(1,steps);
end
if CheckMass
    massup      = zeros(1,steps);
    massdown    = zeros(1,steps);
    massformula = zeros(1,steps);
end
if CheckMomentum
    momentumup   = zeros(1,steps);
    momentumdown = zeros(1,steps);
end
if CentreofMass
    CoMup      = zeros(steps,d);
    CoMdown    = zeros(steps,d);
    CoMformula = zeros(steps,d);
end
if CentreofMomentum
    CoMomup   = zeros(steps,d);
    CoMomdown = zeros(steps,d);
end

% Start the progress bar
if Waitbar
    wbe = waitbar(0,message{:});
end

%% Perform the Dynamics
% do half a timestep for the KE to enable faster Strang splitting
% ie we do half a KE, then N-1 full V / KE steps, then 1 full V then a
% final half KE.  This reduces the number of Fourier transforms necessary.

% do initial half timestep for KE on both levels

psiHatPlus    = kdiag1.*psiHatPlus;
psiHatMinus   = kdiag1.*psiHatMinus;
psiHatFormula = kdiag1.*psiHatFormula;

% transform to position space

psiPlus    = eIFTn(psiHatPlus,Numerics);
psiMinus   = eIFTn(psiHatMinus,Numerics);
psiFormula = eIFTn(psiHatFormula,Numerics);

% Set up the animation file
if SaveAnimation
    filename = [DataFolder filesep  getTimeStr() '_ExactWithFormula'];
    
    %# create AVI object
    vidObj = VideoWriter([ filename '.avi']);
    vidObj.Quality = 100;
    vidObj.FrameRate = 60;
    open(vidObj);
    
    axLims = [-15 15 -15 15];
end
% Do full time steps
for t=1:steps
%% Forward one timestep
    % Advance the progress bar
    if Waitbar
        waitbar( t/steps, wbe, message{:});
    end
        
    % V operator time step
    psiPlusTemp = aV.*psiPlus+bV.*psiMinus;
    psiMinus    = cV.*psiPlus+dV.*psiMinus;
    psiPlus     = psiPlusTemp;
    psiFormula  = expVF.*psiFormula;
    
    % transform to momentum space
    psiHatPlus    = eFTn(psiPlus,Numerics);
    psiHatMinus   = eFTn(psiMinus,Numerics);
    psiHatFormula = eFTn(psiFormula,Numerics);
    
    if(t == steps)
        psiHatPlus    = kdiag1.*psiHatPlus;
        psiHatMinus   = kdiag1.*psiHatMinus;
        psiHatFormula = kdiag1.*psiHatFormula;
    else
        psiHatPlus    = kdiag2.*psiHatPlus;
        psiHatMinus   = kdiag2.*psiHatMinus;
        psiHatFormula = kdiag2.*psiHatFormula;
    end

    if(t == ACTimesPos)
        
        disp('Adding formula wavepacket');
        
        % KE back half a time step to correct time
        psiHatFormula = psiHatFormula./kdiag1;
        
        % add in formula wavepacket
        
        psiHatFormula(:) = psiHatFormula(:) + ACPsiHatFormula(:);
                
        % put back in sync with two-level calculation
        psiHatFormula = kdiag1.*psiHatFormula;
    end

    % transform to position space
    psiPlus    = eIFTn(psiHatPlus,Numerics);
    psiMinus   = eIFTn(psiHatMinus,Numerics); 
    psiFormula = eIFTn(psiHatFormula,Numerics); 
    
%% Checks 
    % Use the adiabatic representation to run checks 
    if CheckEnergy || CheckMass || CheckMomentum || CentreofMass || CentreofMomentum || PlotAnimation 
        psiUp      = Phi1plus.*psiPlus+Phi1minus.*psiMinus;
        psiDown    = Phi2plus.*psiPlus+Phi2minus.*psiMinus;
        psiHatUp   = eFTn(psiUp,Numerics);
        psiHatDown = eFTn(psiDown,Numerics);
    end

    if CheckEnergy   
        Z1up   = (abs(psiUp).^2).*V1+(abs(psiDown).^2).*V12; % PE up
        Z2up   = (abs(psiHatUp).^2).*(sum(pg.^2,d+1)/2); % KE up
        Z1down = (abs(psiUp).^2).*V12+(abs(psiDown).^2).*V2; % PE down
        Z2down = (abs(psiHatDown).^2).*(sum(pg.^2,d+1)/2); % KE down

        energyup(t)   = dx^d*sum(Z1up(:))+dp^d*sum(Z2up(:)); % Total up
        energydown(t) = dx^d*sum(Z1down(:))+dp^d*sum(Z2down(:)); % Total down
    end
    if CheckMass
        massup(t)      = sqrt(dx^d*sum(abs(psiUp(:)).^2));
        massdown(t)    = sqrt(dx^d*sum(abs(psiDown(:)).^2));
        massformula(t) = sqrt(dx^d*sum(abs(psiFormula(:)).^2));
    end
    if CheckMomentum
        momentumup(t)   = sqrt(dp^d*sum(abs(psiHatUp(:)).^2));
        momentumdown(t) = sqrt(dp^d*sum(abs(psiHatDown(:)).^2));
    end
    if CentreofMass
        Mup      = dx^d*sum(abs(psiUp(:)).^2);
        Mdown    = dx^d*sum(abs(psiDown(:)).^2);
        Mformula = dx^d*sum(abs(psiFormula(:)).^2);
        for dim = 1:d
            integrandup       = abs(psiUp).^2.*repGrid{dim};
            CoMup(t,dim)      = 1/Mup.*dx^d*sum(integrandup(:));
            integranddown     = abs(psiDown).^2.*repGrid{dim};
            CoMdown(t,dim)    = 1/Mdown.*dx^d*sum(integranddown(:));
            integrandformula  = abs(psiFormula).^2.*repGrid{dim};
            CoMformula(t,dim) = 1/Mformula.*dx^d*sum(integrandformula(:));
        end
        
    end
    if CentreofMomentum
        Mup   = dp^d*sum(abs(psiHatUp(:)).^2);
        Mdown = dp^d*sum(abs(psiHatDown(:)).^2);
        for dim = 1:d
            integrandup      = abs(psiHatUp).^2.*repGrid{dim};
            CoMomup(t,dim)   = 1/Mup.*dp^d*sum(integrandup(:));
            integranddown    = abs(psiHatDown).^2.*repGrid{dim};
            CoMomdown(t,dim) = 1/Mdown.*dp^d*sum(integranddown(:));
        end
    end
%% Plotting and Saving
    
    if PlotAnimation
        if d==2
            % Note: contourm has the same problem as meshgrid! Hence
            % transposes
            % mask wavepackets to speed up plotting
            [xgMUp,ygMUp,psiMUp] = maskPsi(psiUp,x,dx,10^(-3));
            [xgMDown,ygMDown,psiMDown] = maskPsi(psiDown,x,dx,10^(-3));
                       
            if max(abs(psiFormula)) == 0
                xgMFormula = 0;
                ygMFormula = 0;
                psiMFormula = 0;
            else
                [xgMFormula,ygMFormula,psiMFormula] = maskPsi(psiFormula,x,dx,10^(-3));
            end
            
            if t == 1
                % Initialize figure
                Vmin = min(-Rho(:)+Tr(:));
                Vmax = max(Rho(:)+Tr(:));

                %create figure and store handle
                hF2LWF = figure('units','normalized','outerposition',[0 0 1 1]);
                set(hF2LWF,'color','w')
                axis off
                title('2D Non-adiabatic transition')

                % VU axis and plot
                hAxesV1 = axes('Position',[0.1,0.25,0.35,0.65]);
                hAxesV1.FontSize = 7;
                VPlot1 = contourm(xg(:,:,1),xg(:,:,2),(Rho+Tr).',20);
                axis(axLims);
                title('VU','FontSize',12)
                xlabel('x')
                ylabel('y')
                colormap(hAxesV1,hot);
                caxis(hAxesV1,[Vmin Vmax]);

                % VL axis and plot
                hAxesV2 = axes('Position',[0.6,0.25,0.35,0.65]);
                hAxesV2.FontSize = 7;
                VPlot2 = contourm(xg(:,:,1),xg(:,:,2),(-Rho+Tr).',20);
                axis(axLims);
                title('VL','FontSize',12)
                xlabel('x')
                ylabel('y')
                colormap(hAxesV2,hot);
                caxis(hAxesV2,[Vmin Vmax]);

                % V colorbar for potential plots
                cbP = colorbar(hAxesV2,'Position',[0.5, 0.3 , 0.05, 0.55]);
            else
                figure(hF2LWF)
                % Delete psiU, psiD, formula and legend axes
                delete(hAxesWP1)
                delete(hAxesWP2)  
                delete(hAxesFormula)
                delete(hAxesLegend)
            end
            % psiU axis
            hAxesWP1 = axes('Position',[0.1,0.25,0.35,0.65]);
            axis(hAxesWP1,'off') %set visibility for axes to 'off'
            hAxesWP1.FontSize = 7;
            colormap(hAxesWP1,cool); % set colormap
            axis(axLims);
            WPPlot1 = contourm(xgMUp,ygMUp,abs(psiMUp).','LevelList',[(0.1:0.1:1)*boundU]);

            % psiU colorbar
            cbpsiU = colorbar(hAxesWP1,'Location','SouthOutside');
            set(cbpsiU, 'Position',[0.1,0.1,0.35,0.05]);
            title(cbpsiU,'\psi^U','FontSize',12)
            caxis(hAxesWP1,[0 boundU]);

            % psiD axis
            hAxesWP2 = axes('Position',[0.6,0.25,0.35,0.65]);
            axis(hAxesWP2,'off') %set visibility for axes to 'off'
            hAxesWP2.FontSize = 7;
            colormap(hAxesWP2,cool); %set colormap
            axis(axLims);
            WPPlot2 = contourm(xgMDown,ygMDown,abs(psiMDown).','LevelList',[(0.1:0.1:1)*boundD]);

            % psiD colorbar
            cbpsiD = colorbar(hAxesWP2,'Location','SouthOutside');
            set(cbpsiD, 'Position',[0.6,0.1,0.35,0.05]);
            title(cbpsiD,'\psi^D','FontSize',12)
            caxis(hAxesWP2,[0 boundD]); 

            if(strcmp(direction,'down'))
                % formula axis
                hAxesFormula = axes('Position',[0.6,0.25,0.35,0.65]);
                axis(hAxesFormula,'off') %set visibility for axes to 'off'
                hAxesFormula.FontSize = 7;
                FormulaPlot = contourm(xgMFormula,ygMFormula,abs(psiMFormula).','LineStyle',':','LineColor','r','LevelList',[(0.1:0.1:1)*boundD]);
                axis(axLims);
                
                % Legend on separate axis
                if max(abs(psiMFormula))>0
                    hAxesLegend = axes('Position',[0.6,0.25,0.35,0.65]);
                    LegendPlot = plot(NaN,NaN,':r');
                    axis(axLims);
                    axis(hAxesLegend,'off') %set visibility for axes to 'off'
                    hAxesLegend.FontSize = 7;
                    legend(LegendPlot,'\psi^f','Location','southeast')
                else
                    hAxesLegend = axes('Position',[2 2 2 2]);
                end
            else
                hAxesFormula = axes('Position',[0.1,0.25,0.35,0.65]);
                axis(hAxesFormula,'off') %set visibility for axes to 'off'
                hAxesFormula.FontSize = 7;
                FormulaPlot = contourm(xgMFormula,ygMFormula,abs(psiMFormula).','LineStyle','--','LineColor','r','LevelList',[(0.1:0.1:1)*boundU]);
                axis(axLims);
                
                % Legend on separate axis
                if max(abs(psiMFormula))>0
                    hAxesLegend = axes('Position',[0.6,0.25,0.35,0.65]);
                    LegendPlot = plot(NaN,NaN,':r');
                    axis(axLims);
                    axis(hAxesLegend,'off') %set visibility for axes to 'off'
                    hAxesLegend.FontSize = 7;
                    legend(LegendPlot,'\psi^f','Location','southeast')
                else
                    hAxesLegend = axes('Position',[2 2 2 2]);
                end
            end
            % link all axes together
            linkaxes([hAxesV1,hAxesV2,hAxesWP1,hAxesWP2,hAxesFormula]);
        elseif d==1
            figure(1)
            subplot(2,1,1)
            plot(xg,abs(psiUp))
            ylim([0 boundU])
            subplot(2,1,2)
            plot(xg,abs(psiDown))
            ylim([0 boundD])
            
            if(strcmp(direction,'down'))
                subplot(2,1,2)
                hold on
                plot(xg,abs(psiFormula),'--r');
                hold off
            else
                subplot(2,1,1)
                hold on
                plot(xg,abs(psiFormula),'--r');
                hold off
            end
            drawnow
        end

        if SaveAnimation
            writeVideo(vidObj, getframe(hF2LWF));
        end
    end
end

psiUp         = Phi1plus.*psiPlus + Phi1minus.*psiMinus;
psiDown       = Phi2plus.*psiPlus + Phi2minus.*psiMinus;
psiHatUp      = eFTn(psiUp,Numerics);
psiHatDown    = eFTn(psiDown,Numerics);
psiHatFormula = eFTn(psiFormula,Numerics);

% Close the progress bar
if(exist('wbe','var'))
    close(wbe);
end

%% Save results in Wavepacket struct
Wavepackets.PsiUp         = psiUp;
Wavepackets.PsiDown       = psiDown;
Wavepackets.PsiHatUp      = psiHatUp;
Wavepackets.PsiHatDown    = psiHatDown;
Wavepackets.PsiHatFormula = psiHatFormula;

%% Save Checks in Wavepacket struct
if CheckEnergy
    Wavepackets.EnergyUp   = energyup;
    Wavepackets.EnergyDown = energydown;
end
if CheckMass
    Wavepackets.MassUp      = massup;
    Wavepackets.MassDown    = massdown;
    Wavepackets.MassFormula = massformula;
end
if CheckMomentum
    Wavepackets.MomentumUp   = momentumup;
    Wavepackets.MomentumDown = momentumdown;
end
if CentreofMass
    CoMupSep                 = mat2cell(CoMup,steps,ones(d,1));
    Wavepackets.CoMup        = [CoMup,makeVU(Potentials,CoMupSep{:})];
    CoMdownSep               = mat2cell(CoMdown,steps,ones(d,1));
    Wavepackets.CoMdown      = [CoMdown,makeVL(Potentials,CoMdownSep{:})];
    CoMformulaSep            = mat2cell(CoMformula,steps,ones(d,1));
    if(strcmp(direction,'down'))
        Wavepackets.CoMformula   = [CoMformula,makeVL(Potentials,CoMformulaSep{:})];
    else
        Wavepackets.CoMformula   = [CoMformula,makeVU(Potentials,CoMformulaSep{:})];
    end
end
if CentreofMomentum==1
    CoMomupSep            = mat2cell(CoMomup,steps,ones(d,1));
    Wavepackets.CoMomup   = [CoMomup,makeVU(Potentials,CoMomupSep{:})];
    CoMomdownSep          = mat2cell(CoMomdown,steps,ones(d,1));
    Wavepackets.CoMomdown = [CoMomdown,makeVL(Potentials,CoMomdownSep{:})];
end

if SaveAnimation == 1
    close(vidObj);
    disp(['Animation saved in ' DataFolder filesep getTimeStr() '_ExactWithFormula'])
end
end
function WavepacketsAC = DetectAvoidedCrossing(WPN,OutputOptions)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Evolve a wavepacket on a nD Potential V until it reaches a local        %
% minimum, store the time and position, apply formula, and terminate.     %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Unload Output Options
PlotAnimation = OutputOptions.PlotAnimation;
SaveAnimation = OutputOptions.SaveAnimation;
Waitbar       = OutputOptions.Waitbar;
DataFolder    = OutputOptions.DataDirO;

[Wavepackets,Potentials,Numerics] = unpackWPN(WPN);
[x,p,xg,pg] = makeXP(Numerics);

Numerics.x  = x;
Numerics.p  = p;
Numerics.xg = xg;
Numerics.pg = pg;

% Unload Numerics
time    = Numerics.time; 
dt      = Numerics.timestep;
d       = Numerics.d; 
dx      = Numerics.xStep; 
dp      = Numerics.pStep;
NumPts  = Numerics.NumPoints;
epsilon = Numerics.epsilon;

% Formula function
formulaFn = str2func(Numerics.formula);
direction = Numerics.formulaOptions.direction;

% Potentials
repGridx = replicateGrid(xg,Numerics);
repGridp = replicateGrid(pg,Numerics);

Vfunc = str2func(Potentials.V);
V     = Vfunc(Potentials,repGridx{:});
rho   = makeRho(Potentials,repGridx{:});

% Initial wavepackets
if ( isfield(Wavepackets,'Psi') && ~isempty(Wavepackets.Psi) )
    if( ischar(Wavepackets.Psi) )
        psi = makePsiFull(Wavepackets.Psi,Wavepackets,Numerics);
    else
        psi = Wavepackets.Psi;
    end
    psiHat = eFTn(psi,Numerics);
else
    if(ischar(Wavepackets.PsiHat))
        psiHat = makePsiHatFull(Wavepackets.PsiHat,Wavepackets,Numerics);
    else
        psiHat = Wavepackets.PsiHat;
    end
    psi=eIFTn(psiHat,Numerics);
end

% bounds for plotting
boundPsi = 2*max(abs(psi(:)));

% determine whether we're evolving forwards or backwards
% tDir is the sign change for the forwards in time eFT
if(time<0)
    tDir    = -1;
    message = {'Backwards Evolution on V'};
else
    tDir    = 1;
    message = {'Forwards Evolution on V'};
end

% KE and V evolution operators
kdiag = exp(-1i*tDir*dt * (sum(pg.^2,d+1))/(2*epsilon)/2); %half a timestep
expV  = exp(-1i*tDir*dt * V/epsilon); % full timestep

% Get number of time steps
steps = round(abs(time)/dt);

% Begin the waitbar    
if Waitbar
    wbe = waitbar(0,message{:});
end

if SaveAnimation
    filename = [DataFolder filesep  getTimeStr() '_ACDetection'];
    
    %# create AVI object
    vidObj = VideoWriter([ filename '.avi']);
    vidObj.Quality = 100;
    vidObj.FrameRate = 60;
    open(vidObj);
    
    axLims = [-15 15 -15 15];
end

% find initial CoM
CoM    = getCoM(psi);
CoMSep = num2cell(CoM);

%% Perform the dynamics

noAC              = 0;
ACTime           = [];
ACTimePos        = [];

ACCoM             = zeros(d,1);
ACCoMom           = zeros(d,1);

Input             = num2cell(NumPts.*ones(d,1));
ACPsiHat          = zeros(Input{:},0);
ACPsiHatFormula   = zeros(Input{:},0);
WavepacketsF      = Wavepackets;

rhoCoM = zeros(steps,1);

for t=1:steps
    % Advance waitbar
    if Waitbar
        waitbar( t/steps, wbe, message{:});
    end  

    % half timestep for KE
    psiHat = kdiag.*psiHat;

    % transform to position space
    psi = eIFTn(psiHat,Numerics);

    % full timestep V
    psi = expV.*psi;
    
    % transform to momentum space
    psiHat = eFTn(psi,Numerics);  
    
    % half timestep KE
    psiHat = kdiag.*psiHat;
      
    % transform to position space
    psi = eIFTn(psiHat,Numerics);

    % find centre of mass on rho
    CoM = getCoM(psi);
    
    CoMSep = num2cell(CoM);
    
    rhoCoM(t) = makeRho(Potentials,CoMSep{:});
    
    % detect avoided crossing at previous time step
    if t>2 && (rhoCoM(t-2) > rhoCoM(t-1)) && (rhoCoM(t) > rhoCoM(t-1)) ...
             &&  (rhoCoM(t) - rhoCoM(t-1) > 10^(-12))
        
        noAC = noAC + 1;
        
        % minimum was at previous timestep
        ACTime    = tDir*(t-1)*dt;
        ACTimePos = t-1;

        psiHatAC = psiHatm1;
        psiAC    = eIFTn(psiHatAC,Numerics);
       
        % apply formula
        ACCoM   = getCoM(psiAC);
        ACCoMom = getCoMom(psiHatAC);
        
        if(strcmp(direction,'down'))
            WavepacketsF.PsiHatUpCrossing   = psiHatAC;
        else
            WavepacketsF.PsiHatDownCrossing = psiHatAC;
        end
        
        WavepacketsF.CoM    = ACCoM;
        WavepacketsF.CoMom = ACCoMom;
        
        WavepacketsF.times = 0:dt:dt*t;
        
        WPNF = packWPN(WavepacketsF,Potentials,Numerics);
        
        psiHatACFormula = formulaFn(WPNF,OutputOptions);
            
        %% Plotting
        if PlotAnimation
            if d == 1
                % Plot wavepacket in third subplot
                subplot(3,1,3)
                plot(p,abs(psiHatACFormula));
                hold all
            elseif d==2
                % Note: contourm has the same problem as meshgrid! Hence
                % transposes
                % plot wavepacket in second plot
                figure(hFAC)
                
                % Mask psiFormula to speed plotting
                [pgMFormula,qgMFormula,psiHatMFormula] = maskPsi(psiHatACFormula,p,dp,10^(-3));
                
                if noAC == 1
                    % Construct right axes
                    hAxesFormula = axes('Position',[0.6,0.25,0.35,0.65]);
                    axis(hAxesFormula,'off') %set visibility for axes to 'off'
                    hAxesFormula.FontSize = 7;
                    colormap(hAxesFormula,cool); % set colormap
                    boundFormula = 2*max(abs(psiHatMFormula(:)));
                    
                    WPPlotFormula = contour(qgMFormula,pgMFormula,abs(psiHatMFormula).',10);
                    hAxesFormula.FontSize = 7;
                    title('Transmitted Wavepacket(s)','FontSize',12)
                    
                    % psiD colorbar
                    cbpsiFormula = colorbar(hAxesFormula,'Location','SouthOutside','FontSize',7);
                    set(cbpsiFormula, 'Position',[0.6,0.1,0.35,0.05]);
                    title(cbpsiFormula,'\psi^f','FontSize',12)
                    xlabel('p')
                    ylabel('q')
                    caxis(hAxesFormula,[0 boundFormula])

                else
                    hold(hAxesFormula,'on')
                    % Add new wavepacket to formula plot
                    WPPlotFormula = contour(hAxesFormula,qgMFormula,pgMFormula,abs(psiHatMFormula),10);
                end
                % link all the axes together on one figure
                linkaxes([hAxesV,hAxesWP,hAxesFormula]);
            end
        end
        
        ACPsiHat = cat(d+1,ACPsiHat,psiHatAC);
        ACPsiHatFormula = cat(d+1,ACPsiHatFormula,psiHatACFormula);
        
        disp(['Avoided crossing detected at ' num2str(ACCoM(:,noAC).') ' with momentum ' num2str(ACCoMom(:,noAC).')]);
        break
    end
    
    % prepare for next time step
    psiHatm1 = psiHat;
    
    %% Plotting
    if PlotAnimation
        if d == 2
            % Note: contourm has the same problem as meshgrid! Hence
            % transposes
            [xgM,ygM,psiM] = maskPsi(psi,x,dx,10^(-3));
            if t==1
                % Bounds for colorbar
                Vmin = min(V(:));
                Vmax = max(V(:));

                % create figure and store handle
                hFAC = figure('units','normalized','outerposition',[0 0 1 1]);
                set(hFAC,'color','w')
                axis off
                title('Avoided Crossing Detection')

                % VU axes and plot
                hAxesV = axes('Position',[0.1,0.25,0.35,0.65]);
                hAxesV.FontSize = 7;
                VPlot = contourm(xg(:,:,1),xg(:,:,2),V.',20);
                axis(axLims)
                title('VU','FontSize',12)
                xlabel('x')
                ylabel('y')
                colormap(hAxesV,hot);
                caxis(hAxesV,[Vmin Vmax]);

                % V colorbar for potential plots
                cbP = colorbar(hAxesV,'Position',[0.5, 0.3 , 0.05, 0.55]);
            else
                figure(hFAC)
                % Delete psi axes
                delete(hAxesWP)
            end
            % Construct psi axes
            hAxesWP = axes('Position',[0.1,0.25,0.35,0.65]);
            axis(hAxesWP,'off') %set visibility for axes to 'off'
            hAxesWP.FontSize = 7;
            colormap(hAxesWP,cool); %set colormap
            WPPlot1 = contourm(xgM,ygM,abs(psiM).',10);
            axis(axLims)

            % psiU colorbar
            cbpsiU = colorbar(hAxesWP,'Location','SouthOutside');
            set(cbpsiU, 'Position',[0.1,0.1,0.35,0.05]);
            title(cbpsiU,'\psi^U','FontSize',12)
            caxis(hAxesWP,[0 boundPsi]);

            % Link axes together
            if noAC == 0
                linkaxes([hAxesV,hAxesWP]);
            else
                linkaxes([hAxesV,hAxesWP,hAxesFormula]);
            end
        elseif d==1
            subplot(3,1,1)
            plot(x,abs(psi));
            subplot(3,1,2)
            plot(x,rho);
        else
            message('I cannot plot in higher dimensions');
        end
        if SaveAnimation
           writeVideo(vidObj, getframe(hFAC)); 
        end
    end
end

% wavepacket does not reach minimum by predicted time
if noAC == 0
    % Return empty wavepackets and time
    ACTime         = [];
    ACTimePos      = [];
    ACCoM           = [];
    ACCoMom        = [];
    ACPsiHat        = [];
    ACPsiHatFormula = [];
end

if Waitbar
    close(wbe);
end

WavepacketsAC.ACPsiHat        = ACPsiHat;
WavepacketsAC.ACPsiHatFormula = ACPsiHatFormula;
WavepacketsAC.ACTime          = ACTime;
WavepacketsAC.ACTimePos       = ACTimePos;
WavepacketsAC.ACCoM           = ACCoM;
WavepacketsAC.ACCoMMom        = ACCoMom;
WavepacketsAC.noAC            = noAC;
WavepacketsAC.rhoCoM          = rhoCoM;

    function CoM = getCoM(psi)
        M   = dx^d*sum(abs(psi(:)).^2);
        CoM = zeros(d,1);
        for dim=1:d
            integrand = abs(psi).^2.*repGridx{dim};
            CoM(dim)  = 1/M.*dx^d*sum(integrand(:));
        end
    end

    function CoMom = getCoMom(psiHat)
        M      = dp^d*sum(abs(psiHat(:)).^2);
        CoMom = zeros(d,1);
        for dim=1:d
            integrand   = abs(psiHat).^2.*repGridp{dim};
            CoMom(dim) = 1/M.*dp^d*sum(integrand(:));
        end
    end

if SaveAnimation
    close(vidObj);
    disp(['Animation saved in' DataFolder filesep  getTimeStr() '_ACDetection']);
end

end
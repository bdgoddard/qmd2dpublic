function ExpV=makeExpV(PN,OutputOptions)
%% Construct potential operator for two level dynamics in 1D and 2D

Potentials = PN.Potentials;
Numerics   = PN.Numerics;

% Unload Numerics
[~,~,xg,~] = makeXP(Numerics);

dt        = Numerics.timestep;
NumPoints = Numerics.NumPoints; 
d         = Numerics.d;
epsilon   = Numerics.epsilon;
tDir      = Numerics.tDir;

% Make diabatic potentials
repGrid       = replicateGrid(xg,Numerics);
[~,X,Z,trace] = makeRhoXZTrace(Potentials,repGrid{:});

% Output options
Waitbar = OutputOptions.Waitbar;

% Define the V operator in x-space. For each point (x,y), u defines a 2x2
% matrix.
if Waitbar
    wbe=waitbar(0,'Constructing the PE operator');
end

grids = num2cell(NumPoints*ones(d,1));
ExpV  = zeros(2,2,grids{:});
if d == 2    
    for m = 1:NumPoints
        if Waitbar
            waitbar( m/NumPoints, wbe, 'Constructing the PE operator');
        end
        for n = 1:NumPoints   
            h1            = Z(m,n);
            h2            = X(m,n);
            ExpV(:,:,m,n) = expm((-1i*tDir*dt)*[h1+trace(m,n), h2; h2, -h1+trace(m,n)]/epsilon); % full timestep
        end
    end
elseif d == 1
    for n = 1:NumPoints   
        if Waitbar
            waitbar( n/NumPoints, wbe, 'Constructing the PE operator');
        end
        h1          = Z(n);
        h2          = X(n);
        ExpV(:,:,n) = expm((-1i*tDir*dt)*[h1+trace(n), h2; h2, -h1+trace(n)]/epsilon); % full timestep
    end
end
if Waitbar
    close(wbe)
end
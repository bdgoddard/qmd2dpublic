function  Wavepackets = ExactTwoLevel(WPN,OutputOptions)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculate the evolution of the wavepacket on a 2D potential V.  Output  %
% is two wavepackets in momentum space, and two in position space, with   % 
% optional checks results.                                                %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Output options
Waitbar          = OutputOptions.Waitbar;
CheckEnergy      = OutputOptions.CheckTotalEnergy; 
CheckMass        = OutputOptions.CheckTotalMass;
CheckMomentum    = OutputOptions.CheckTotalMomentum;
CentreofMass     = OutputOptions.CheckCOM;
CentreofMomentum = OutputOptions.CheckCOMom;
PlotAnimation    = OutputOptions.PlotAnimation; 
SaveAnimation    = OutputOptions.SaveAnimation;
DataFolder    = OutputOptions.DataDirO;

[Wavepackets,Potentials,Numerics] = unpackWPN(WPN);

%% Initialisation

% Unload Numerics
[x,p,xg,pg] = makeXP(Numerics);
Numerics.x  = x;
Numerics.p  = p;
Numerics.xg = xg;
Numerics.pg = pg;

time      = Numerics.time; 
dt        = Numerics.timestep;
d         = Numerics.d; 
dx        = Numerics.xStep; 
dp        = Numerics.pStep;
epsilon   = Numerics.epsilon;
direction = Numerics.formulaOptions.direction;

% Construct diabatic potentials
repGrid     = replicateGrid(xg,Numerics);
[V1,V2,V12] = makeV1V2V12(Potentials,repGrid{:});

if (~isempty(Wavepackets.PsiUp) || ~isempty(Wavepackets.PsiDown) )
    initialSpace = true;
else
    initialSpace = false;
end

% Initial wavepackets
if initialSpace % have to define PsiInitial or PsiHatInitial for one level dynamics
    psiUp      = Wavepackets.PsiUp;
    psiDown    = Wavepackets.PsiDown;
    psiHatUp   = eFTn(psiUp,Numerics);
    psiHatDown = eFTn(psiDown,Numerics);
else
    if(strcmp(direction,'down'))
        if(ischar(Wavepackets.PsiHatUp))
            psiHatUp = makePsiHatFull(Wavepackets.PsiHatUp,Wavepackets,Numerics);
        else
            psiHatUp = Wavepackets.PsiHatUp;
        end
        psiHatDown = zeros(size(psiHatUp));
    else
        if(ischar(Wavepackets.PsiHatDown))
            Wavepackets.PsiHatDown
            psiHatDown = makePsiHatFull(Wavepackets.PsiHatDown,Wavepackets,Numerics);
        else
            psiHatDown = Wavepackets.PsiHatDown;
        end
        psiHatUp = zeros(size(psiHatDown));
    end
    
    psiUp      = eIFTn(psiHatUp,Numerics);
    psiDown    = eIFTn(psiHatDown,Numerics);
end

% Plotting bounds
if PlotAnimation
    if d == 1
        if(strcmp(direction,'down'))
            pI = dp*sum(p.*abs(psiHatUp(:)).^2);
            xI = dx*sum(x.*abs(psiUp(:)).^2);
        else
            pI = dp*sum(p.*abs(psiHatDown(:)).^2);
            xI = dx*sum(x.*abs(psiDown(:)).^2);
        end

        VI     = makeRho(Potentials,xI);
        V0     = makeRho(Potentials,0);
        deltaV = VI - V0;
        p0     = sign(pI)*sqrt(pI^2 + 2*deltaV);

        expFactor = abs(ExpFormula1D(WPN,p0));

        if(strcmp(direction,'down'))
            boundU = 2 * max(abs(psiHatUp(:)));
            boundD = min(10 * expFactor * boundU, boundU);
        else
            boundD = 2 * max(abs(psiHatDown(:)));
            boundU = min(expFactor * boundD, boundD);
        end
    elseif d==2 
        if(strcmp(direction,'down'))
            boundU = 2 * max(abs(psiHatUp(:)));
            boundD = 10^(-3)*boundU;
        else
            boundD = 2 * max(abs(psiHatDown(:)));
            boundU = 10^(-3)*boundD;
        end
    end
end

%% Construct the KE and PE operators
% determine whether we're evolving forwards or backwards
% tDir is the sign change for the forwards in time eFT
if (time<0)
    tDir    = -1;
    message = {'Backwards Evolution on V'};
else
    tDir    = 1;
    message = {'Forwards Evolution on V'};
end

% initializing KE and V evolution operators

% KE operators in p-space, half and full time step for Strang Splitting

kdiag1 = exp(-1i*tDir*dt*(sum(pg.^2,d+1))/(2*epsilon)/2); % half a timestep
kdiag2 = exp(-1i*tDir*dt*(sum(pg.^2,d+1))/(2*epsilon)); % full timestep

[Rho,X,Z,Tr] = makeRhoXZTrace(Potentials,repGrid{:});


if(ischar(Numerics.ExpV))
    temp = load(Numerics.ExpV);
    u    = temp.Data;
else
    u = Numerics.ExpV;
end

% to do: generalise to n dimensions
if d == 2    
    % separate u into 4 m x n matrices to avoid a for loop in the dynamics
    aV = squeeze(u(1,1,:,:));
    bV = squeeze(u(1,2,:,:));
    cV = squeeze(u(2,1,:,:));
    dV = squeeze(u(2,2,:,:));
elseif d == 1
    % separate u into 4 m x n matrices to avoid a for loop in the dynamics
    aV = squeeze(u(1,1,:));
    bV = squeeze(u(1,2,:));
	cV = squeeze(u(2,1,:));
    dV = squeeze(u(2,2,:));
else
    disp('Higher Dimensions not yet supported')
end

%% Move to the Diabatic regime using eigenfunctions

% deal with the case when X is close to zero
mask    = abs(X)<10^(-15);
maskPos = mask & (Z>=0);
maskNeg = mask & (Z<0);

% upper level
Phi1plus  = (Z+sqrt(Z.^2+X.^2))./X;
% (Z + abs(Z))/X + O(X)
Phi1minus = ones(size(Phi1plus));

% deal with region where X is small
Phi1plus(maskPos)  = 1;
Phi1minus(maskPos) = 0;
Phi1plus(maskNeg)  = 0;
Phi1minus(maskNeg) = 1;

% normalization
k1        = 1./sqrt(Phi1plus.^2+Phi1minus.^2);
Phi1plus  = k1.*Phi1plus;
Phi1minus = k1.*Phi1minus;

% lower level
Phi2plus  = (Z-sqrt(Z.^2+X.^2))./X;
% (Z - abs(Z))/X + O(X)
Phi2minus = ones(size(Phi2plus));

% deal with region where X is small
Phi2plus(maskPos)  = 0;
Phi2minus(maskPos) = 1;
Phi2plus(maskNeg)  = -1;
Phi2minus(maskNeg) = 0;

% normalization
k2        = 1./sqrt(Phi2plus.^2+Phi2minus.^2);
Phi2plus  = k2.*Phi2plus;
Phi2minus = k2.*Phi2minus;

% The diabatic wavefunctions
if(strcmp(direction,'down'))
    psiPlus  = psiUp.*Phi1plus;
    psiMinus = psiUp.*Phi1minus; % Assumes there is nothing on lower level.
else
    psiPlus  = psiDown.*Phi2plus;
    psiMinus = psiDown.*Phi2minus; % Assumes there is nothing on upper level.
end
    
psiHatPlus  = eFTn(psiPlus,Numerics);
psiHatMinus = eFTn(psiMinus,Numerics);

% Get number of time steps
steps = round(abs(time)/dt);
  
%% Initialize Checks
if CheckEnergy
    energyup   = zeros(1,steps);
    energydown = zeros(1,steps);
end
if CheckMass
    massup   = zeros(1,steps);
    massdown = zeros(1,steps);
end
if CheckMomentum
    momentumup   = zeros(1,steps);
    momentumdown = zeros(1,steps);
end
if CentreofMass
    CoMup   = zeros(steps,d);
    CoMdown = zeros(steps,d);
end
if CentreofMomentum
    CoMomup   = zeros(steps,d);
    CoMomdown = zeros(steps,d);
end

% Start the progress bar
if Waitbar
    wbe = waitbar(0,message{:});
end

%% Perform the Dynamics
% do half a timestep for the KE to enable faster Strang splitting
% ie we do half a KE, then N-1 full V / KE steps, then 1 full V then a
% final half KE.  This reduces the number of Fourier transforms necessary.

% do initial half timestep for KE on both levels

psiHatPlus  = kdiag1.*psiHatPlus;
psiHatMinus = kdiag1.*psiHatMinus;

% transform to position space

psiPlus  = eIFTn(psiHatPlus,Numerics);
psiMinus = eIFTn(psiHatMinus,Numerics);

% Set up the animation file
if SaveAnimation
    filename = [DataFolder filesep  getTimeStr() '_ExactTwoLevel'];
    
    % create AVI object
    vidObj = VideoWriter([ filename '.avi']);
    vidObj.Quality = 100;
    vidObj.FrameRate = 60;
    open(vidObj);
    
    axLims = [-15 15 -15 15];
end
% Do full time steps
for t=1:steps
    % Advance the progress bar
    if Waitbar
        waitbar( t/steps, wbe, message{:});
    end
    
    % V operator time step
    psiPlusTemp = aV.*psiPlus+bV.*psiMinus;
    psiMinus    = cV.*psiPlus+dV.*psiMinus;
    psiPlus     = psiPlusTemp;
   
    % transform to momentum space
    psiHatPlus  = eFTn(psiPlus,Numerics);
    psiHatMinus = eFTn(psiMinus,Numerics);
    
    if(t<steps)
        % full timestep KE
        psiHatPlus  = kdiag2.*psiHatPlus;
        psiHatMinus = kdiag2.*psiHatMinus;
    else
        % half timestep KE for final step
        psiHatPlus  = kdiag1.*psiHatPlus;
        psiHatMinus = kdiag1.*psiHatMinus;
    end
   
    % transform to momentum space
    psiPlus  = eIFTn(psiHatPlus,Numerics);
    psiMinus = eIFTn(psiHatMinus,Numerics); 
    
%% Checks
    % Use the adiabatic representation to run checks 
    if CheckEnergy==1 || CheckMass==1 || CheckMomentum==1 || CentreofMass==1  || CentreofMomentum==1  || PlotAnimation==1 
        psiUp      = Phi1plus.*psiPlus+Phi1minus.*psiMinus;
        psiDown    = Phi2plus.*psiPlus+Phi2minus.*psiMinus;
        psiHatUp   = eFTn(psiUp,Numerics);
        psiHatDown = eFTn(psiDown,Numerics);
    end

    if CheckEnergy == 1   
        Z1up          = (abs(psiUp).^2).*V1+(abs(psiDown).^2).*V12; % PE up
        Z2up          = (abs(psiHatUp).^2).*(sum(pg.^2,d+1)/2); % KE up
        Z1down        = (abs(psiUp).^2).*conj(V12)+(abs(psiDown).^2).*V2; % PE down
        Z2down        = (abs(psiHatDown).^2).*(sum(pg.^2,d+1)/2); % KE down

        energyup(t)   = dx^d*sum(Z1up(:))+dp^d*sum(Z2up(:)); % Total up
        energydown(t) = dx^d*sum(Z1down(:))+dp^d*sum(Z2down(:)); % Total down
    end
    if CheckMass == 1
        massup(t)   = sqrt(dx^d*sum(abs(psiUp(:)).^2));
        massdown(t) = sqrt(dx^d*sum(abs(psiDown(:)).^2));
    end
    if CheckMomentum == 1
        momentumup(t)   = sqrt(dp^d*sum(abs(psiHatUp(:)).^2));
        momentumdown(t) = sqrt(dp^d*sum(abs(psiHatDown(:)).^2));
    end
    if CentreofMass == 1
        Mup   = dx^d*sum(abs(psiUp(:)).^2);
        Mdown = dx^d*sum(abs(psiDown(:)).^2);
        for dim=1:d
            integrandup    = abs(psiUp).^2.*repGrid{dim};
            CoMup(t,dim)   = 1/Mup.*dx^d*sum(integrandup(:));
            integranddown  = abs(psiDown).^2.*repGrid{dim};
            CoMdown(t,dim) = 1/Mdown.*dx^d*sum(integranddown(:));
        end
    end
    if CentreofMomentum == 1
        Mup   = dp^d*sum(abs(psiHatUp(:)).^2);
        Mdown = dp^d*sum(abs(psiHatDown(:)).^2);
        for dim=1:d
            integrandup      = abs(psiHatUp).^2.*repGrid{dim};
            CoMomup(t,dim)   = 1/Mup.*dp^d*sum(integrandup(:));
            integranddown    = abs(psiHatDown).^2.*repGrid{dim};
            CoMomdown(t,dim) = 1/Mdown.*dp^d*sum(integranddown(:));
        end
    end
%% Plotting and saving animations
    if PlotAnimation == 1
        if d == 2 
            % Mask wavepackets to speed up plotting
            [xgMUp,ygMUp,psiMUp]       = maskPsi(psiUp,x,dx,10^(-3));
            [xgMDown,ygMDown,psiMDown] = maskPsi(psiDown,x,dx,10^(-3));

            if t == 1
                % Initialize figure and plot potentials
                Vmin = min(-Rho(:)+Tr(:));
                Vmax = max(Rho(:)+Tr(:));

                % Create figure and store handle
                hF2L = figure('units','normalized','outerposition',[0 0 1 1]);
                set(hF2L,'color','w')
                axis off
                title('2D Non-adiabatic transition')

                % VU axes and plot
                hAxesV1 = axes('Position',[0.1,0.25,0.35,0.65]);
                hAxesV1.FontSize = 7;
                VPlot1 = contourm(xg(:,:,1),xg(:,:,2),rho,20);
                axis(axLims)
                title('VU','FontSize',12)
                xlabel('x')
                ylabel('y')
                colormap(hAxesV1,parula);
                caxis(hAxesV1,[VMin Vmax]);

                % VL axes and plot
                hAxesV2 = axes('Position',[0.6,0.25,0.35,0.65]);
                hAxesV2.FontSize = 7;
                VPlot2 = contourm(xg(:,:,1),xg(:,:,2),-rho,20);
                axis(axLims)
                title('VL','FontSize',12)
                xlabel('x')
                ylabel('y')
                colormap(hAxesV1,parula);
                caxis(hAxesV2,[Vmin Vmax]);

                % V colorbar for potential plots
                cbP = colorbar(hAxesV1,'Position',[0.5, 0.3 , 0.05, 0.55]);
            else
                figure(hF2L)
                % Delete psiU and psiD axes
                delete(hAxesWP1)
                delete(hAxesWP2)   
            end
            % psiU axes
            hAxesWP1 = axes('Position',[0.1,0.25,0.35,0.65]);
            axis(hAxesWP1,'off') %set visibility for axes to 'off'
            hAxesWP1.FontSize = 7;
            colormap(hAxesWP1,hot); %set colormap
            axis(axLims)
            WPPlot1 = contourm(xgMUp,ygMUp,abs(psiMUp),10);

            % psiU colorbar
            cbpsiU = colorbar(hAxesWP1,'Location','SouthOutside');
            set(cbpsiU, 'Position',[0.1,0.1,0.35,0.05]);
            title(cbpsiU,'\psi^U','FontSize',12)
            caxis(hAxesWP1,[0 boundU]);

            % psiD axes
            hAxesWP2 = axes('Position',[0.6,0.25,0.35,0.65]);
            axis(hAxesWP2,'off') %set visibility for axes to 'off'
            hAxesWP2.FontSize = 7;
            colormap(hAxesWP2,hot); %set colormap
            axis(axLims)
            WPPlot2 = contourm(xgMDown,ygMDown,abs(psiMDown),10);

            % psiD colorbar
            cbpsiD = colorbar(hAxesWP2,'Location','SouthOutside');
            set(cbpsiD, 'Position',[0.6,0.1,0.35,0.05]);
            title(cbpsiD,'\psi^D','FontSize',12)
            caxis(hAxesWP2,[0 boundD]); 

            % link axes together
            linkaxes([hAxesV1,hAxesV2,hAxesWP1,hAxesWP2]);
        elseif d==1
            figure(1)
            
            subplot(2,1,1)
            plot(xg,abs(psiUp))
            ylim([0 boundU])
            
            subplot(2,1,2)
            plot(xg,abs(psiDown))
            ylim([0 boundD])
            drawnow
        end

        if SaveAnimation
            writeVideo(vidObj, getframe(hF2L));
        end
    end
end

psiUp      = Phi1plus.*psiPlus+Phi1minus.*psiMinus;
psiDown    = Phi2plus.*psiPlus+Phi2minus.*psiMinus;
psiHatUp   = eFTn(psiUp,Numerics);
psiHatDown = eFTn(psiDown,Numerics);

% Close the progress bar
if(exist('wbe','var'))
    close(wbe);
end

%% Save results in Wavepacket struct
Wavepackets.PsiUp      = psiUp;
Wavepackets.PsiDown    = psiDown;
Wavepackets.PsiHatUp   = psiHatUp;
Wavepackets.PsiHatDown = psiHatDown;

%% Save Checks in Wavepacket struct
if CheckEnergy==1
    Wavepackets.EnergyUp   = energyup;
    Wavepackets.EnergyDown = energydown;
end
if CheckMass==1
    Wavepackets.MassUp   = massup;
    Wavepackets.MassDown = massdown;
end
if CheckMomentum==1
    Wavepackets.MomentumUp   = momentumup;
    Wavepackets.MomentumDown = momentumdown;
end
if CentreofMass==1
    CoMupSep            = mat2cell(CoMup,steps,ones(d,1));
    Wavepackets.CoMup   = [CoMup,makeVU(Potentials,CoMupSep{:})];
    CoMdownSep          = mat2cell(CoMdown,steps,ones(d,1));
    Wavepackets.CoMdown = [CoMdown,makeVL(Potentials,CoMdownSep{:})];
end

if CentreofMomentum==1
    CoMomupSep            = mat2cell(CoMomup,steps,ones(d,1));
    Wavepackets.CoMomup   = [CoMomup,makeVU(Potentials,CoMomupSep{:})];
    CoMomdownSep          = mat2cell(CoMomdown,steps,ones(d,1));
    Wavepackets.CoMomdown = [CoMomdown,makeVL(Potentials,CoMomdownSep{:})];
end

if SaveAnimation == 1
    close(vidObj);
    disp(['Animation saved in' DataFolder filesep  getTimeStr() '_ExactTwoLevel']);
end

end
    function V12 = V12Tanh2D(Parameters,varargin)
% One of the two diabatic potential surfaces.

    if(isfield(Parameters,'delta'))
        delta = Parameters.delta;
    else
        delta = 1;
    end
    
    x1 = varargin{1};
    
    if nargin ==3
        x2 = varargin{2};
    else
        x2 = zeros(size(x1));
    end

    V12 = delta*ones(size(x1));

end
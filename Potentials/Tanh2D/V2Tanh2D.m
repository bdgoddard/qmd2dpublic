function V2 = V2Tanh2D(Parameters,varargin)

    if(isfield(Parameters,'alpha'))
        alpha = Parameters.alpha;
    else
        alpha = 1;
    end
    
    if(isfield(Parameters,'beta'))
        beta = Parameters.beta;
    else
        beta = 0.05;
    end
    
    if(isfield(Parameters,'theta'))
        theta = Parameters.theta;
    else
        theta = 0;
    end
    
    if(isfield(Parameters,'gamma'))
        gamma = Parameters.gamma;
    else
        gamma = 1;
    end

    x1 = varargin{1};
    
    if nargin ==3
        x2 = varargin{2};
    else
        x2 = zeros(size(x1));
    end
    
    x =  cos(theta)*x1-sin(theta)*x2;
    y =  sin(theta)*x1+cos(theta)*x2;
    
    V2 = beta*(x.^2 + y.^2) - gamma*tanh(alpha*(x))/2;

end
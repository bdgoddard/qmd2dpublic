    function [Rho,X,Z,Trace]=makeRhoXZTrace(Potentials,varargin)

[V1,V2,X] = makeV1V2V12(Potentials,varargin{:});

Z=(V1-V2)/2;

Trace=(V1+V2)/2;

Rho=(X.^2+Z.^2).^(1/2);

end
function VL=makeVL(Potentials,varargin)

[Rho,~,~,Trace]=makeRhoXZTrace(Potentials,varargin{:});

VL= -Rho + Trace;

end

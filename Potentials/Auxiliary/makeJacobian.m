function DV = makeJacobian(Potentials,varargin)
    % Assumes V2 = - V1

    V1function = str2func(Potentials.V1);
    V12function = str2func(Potentials.V12);

    Parameters = Potentials.Parameters;

    [~,D1V1,D2V1] = V1function(Parameters,varargin{:});
    [~,D1V12,D2V12] = V12function(Parameters,varargin{:});
    
    DV = [D1V1 D2V1; D1V12 D2V12];


end
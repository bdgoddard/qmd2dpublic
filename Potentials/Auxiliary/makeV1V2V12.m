function [V1,V2,V12] = makeV1V2V12(Potentials,varargin)

    V1function = str2func(Potentials.V1);
    V2function = str2func(Potentials.V2);
    V12function = str2func(Potentials.V12);

    Parameters = Potentials.Parameters;

    V1=V1function(Parameters,varargin{:});
    V2=V2function(Parameters,varargin{:});

    V12=V12function(Parameters,varargin{:});


end
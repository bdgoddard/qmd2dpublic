function V2 = V2CJLM1(Parameters,varargin)


if(isfield(Parameters,'delta'))
    delta = Parameters.delta;
else
    delta = 1;
end    

if(isfield(Parameters,'beta'))
    beta = Parameters.beta;
else
    beta = 0.05;
end    

if(isfield(Parameters,'alpha'))
    alpha = Parameters.alpha;
else
    alpha = 1;
end
        
V1 = V1CJLM1(Parameters,varargin{:});

x1 = varargin{1};

if nargin ==3
    x2 = varargin{2};
else
    x2 = zeros(size(x1));
end

V2 = beta*(x1.^2 + x2.^2) - alpha*x1; 

end
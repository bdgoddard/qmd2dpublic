function V12 = V12CJLM1(Parameters,varargin)
% One of the two diabatic potential surfaces.

    if(isfield(Parameters,'delta'))
        delta = Parameters.delta;
    else
        delta = 1;
    end    
    
    if(isfield(Parameters,'beta'))
        beta = Parameters.beta;
    else
        beta = 0.05;
    end        
        
    if(isfield(Parameters,'alpha'))
        alpha = Parameters.alpha;
    else
        alpha = 1;
    end
    
    x1 = varargin{1};
     
    if nargin ==3
        x2 = varargin{2};
    else
        x2 = zeros(size(x1));
    end
    
    V12 = alpha*sqrt(x2.^2+delta.^2);

end
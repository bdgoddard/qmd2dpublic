%% Example 6.2 with parameters (6.9) and intial wavepacket of form (6.1)

clear all

p0  = 5;
q0  = 2;
x20 = 0;

% Output options for plotting (creates video), waitbar and various checks
OutputOptions.PlotAnimation  = 0;
OutputOptions.PlotExactVsFormula = 0;
OutputOptions.SaveAnimation  = 0;
OutputOptions.Waitbar        = 1;
OutputOptions.CheckCOM       = 1;
OutputOptions.CheckTotalMass = 1;
OutputOptions.DataSubDir     = 'CJLM1';

% Numerics
Numerics.epsilon   = 1/30;
Numerics.NumPoints = 2^13;
Numerics.xStart    = -40;
Numerics.xEnd      = 40;
Numerics.d         = 2;
Numerics.timestep  = 1/50/(sqrt(p0^2+q0^2));
Numerics.formulaOptions.direction = 'down';

if Numerics.d == 2
    Numerics.formulaOptions.ExpFunction = 'ExpFormula2DSliceRotated';
    Numerics.formula = 'Formula2DSlicingRotated';
end
Numerics.tBackward = -20/abs(sqrt(p0^2+q0^2));
Numerics.tForward  = 20/abs(sqrt(p0^2+q0^2));

Numerics.formulaOptions.relativeCutoff = 10^-10;

% Potentials
Potentials.V1  = 'V1CJLM1';
Potentials.V2  = 'V2CJLM1';
Potentials.V12 = 'V12CJLM1';
Potentials.Parameters.delta = 0.5;
Potentials.Parameters.beta  = 0;
Potentials.Parameters.alpha = 1;

% Wavepackets
Wavepackets.PsiHatUpCrossing = 'GaussianHat';
Wavepackets.Parameters.p20   = q0;
Wavepackets.Parameters.p10    = p0;
Wavepackets.Parameters.x20   = x20;

Wavepackets.epsilon = Numerics.epsilon;

WPN = packWPN(Wavepackets,Potentials,Numerics);

[WPN,TotalError] = ExactVsFormula(WPN,OutputOptions);

function repGrid = replicateGrid(grid,Numerics)
    % Replicate grid for arguments of functions.
    NumPointRep = num2cell(ones(Numerics.d,1)*Numerics.NumPoints);
    repGrid = mat2cell(grid,NumPointRep{:},ones(Numerics.d,1));
end
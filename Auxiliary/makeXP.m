function [x,p,xg,pg] = makeXP(Numerics)

    NumPoints = Numerics.NumPoints;
    d = Numerics.d;
    xStep  = Numerics.xStep;
    xStart = Numerics.xStart;
    xEnd   = Numerics.xEnd;
    pStep  = Numerics.pStep;

    %%%%%%%%%%%%%%%%%%%%%%%%%%%  Position   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    onesd = ones(1,d);
    
    % set up 1D vector
    x = (xStart : xStep : xEnd).' *onesd; % N x d
    xg = makeGrid(x,NumPoints,d);
    
    % In 2D xg(:,:,1) is constant in rows, xg(:,:,2) is constant in columns
           
    %%%%%%%%%%%%      Momentum (determined by position)    %%%%%%%%%%%%%%%%%%%
    
    % momentum points
    p=(  ( (1:NumPoints).' * onesd )  -1-NumPoints/2).*pStep;
    
    pg = makeGrid(p,NumPoints,d);
    
    %%%%%%%%%%%%           Grid function        %%%%%%%%%%%%%%%%%%%
    
    function vg = makeGrid(v,NumPoints,d)
    
        % Vector to allow dimensional replication
        onesd = ones(1,d);
        
        % Set up empty matrix to hold each coordinate
        grids = num2cell((onesd*NumPoints)); %  1 x d with entries N
        vg = zeros(grids{:},d); % (tensor product) * d

        % Empty cell of matrices to hold output of ndgrid
        Test = mat2cell(vg,grids{:},onesd);

        % Convert matrix into column vectors for input to ndgrid
        vCell = mat2cell(v,NumPoints,onesd);

        % Do tensor produce
        [Test{:}]=ndgrid(vCell{:});

        % Allocate to vg matrix
        for n=1:d
            vg(:,:,n)=Test{n};
        end
 
    end
    
end    
function psiHat = makePsiHatUpCrossingFull(Wavepackets,Numerics)

    [~,~,~,pg] = makeXP(Numerics);

    psiHatFn = str2func(Wavepackets.PsiHatUpCrossing);

    repGrid = replicateGrid(pg,Numerics);
    
    psiHat = psiHatFn(Wavepackets,repGrid{:});

end
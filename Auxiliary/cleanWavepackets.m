function Wavepackets = cleanWavepackets(Wavepackets,ToClear)
% Clears unwanted parts of Wavepackets struct.

    if nargin == 1
        clearList = {'PsiHat','Psi','EnergyOneLevel','MassOneLevel','MomentumOneLevel', ...
        'PsiHatInitial','PsiHatUp','PsiHatDown','PsiUp','PsiDown', ...
        'EnergyUp','EnergyDown','MassUp','MassDown','MomentumUp','MomentumDown'};
        
    else
        clearList = ToClear;
    end

    nClear = length(clearList);
    
    for iClear = 1:nClear
        if(isfield(Wavepackets,clearList{iClear}))
            Wavepackets = rmfield(Wavepackets,clearList{iClear});
        end
    end

end
 
 
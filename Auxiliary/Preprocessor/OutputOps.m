function OutputOptionsOut=OutputOps(OutputOptionsIn)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Constructs/ fills in OutputOptions with defaults.                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

set(0,'defaultaxesfontsize',20);
set(0,'defaulttextfontsize',20);
set(0,'defaultaxeslinewidth',1);
set(0,'defaultlinelinewidth',2);

OutputOptionsOut=struct;
if nargin==0
   	OutputOptionsIn=struct;
	disp('Warning: default output selected')
end

%%%%%%%%%%%%%%%         Data Storage                  %%%%%%%%%%%%%%%%%%%%%

if isfield(OutputOptionsIn,'DataDir')
    OutputOptionsOut.DataDir = OutputOptionsIn.DataDir;
else
    OutputOptionsOut.DataDir = ['..' filesep 'Data' filesep];
end

OutputOptionsOut.DataDirF = [OutputOptionsOut.DataDir 'Formula'];
OutputOptionsOut.DataDirV = [OutputOptionsOut.DataDir 'Potentials'];
OutputOptionsOut.DataDirE = [OutputOptionsOut.DataDir 'Exact'];
OutputOptionsOut.DataDirO = [OutputOptionsOut.DataDir 'Figures'];


if isfield(OutputOptionsIn,'DataSubDir')
    DataSubDir = OutputOptionsIn.DataSubDir;
    OutputOptionsOut.DataDirF = [OutputOptionsOut.DataDirF filesep DataSubDir];
    OutputOptionsOut.DataDirV = [OutputOptionsOut.DataDirV filesep DataSubDir];
    OutputOptionsOut.DataDirE = [OutputOptionsOut.DataDirE filesep DataSubDir];
    OutputOptionsOut.DataDirO = [OutputOptionsOut.DataDirO filesep DataSubDir];
end

if(~exist(OutputOptionsOut.DataDirO,'dir'))
    mkdir(OutputOptionsOut.DataDirO);
end

%%%%%%%%%%%%%%%%              Waitbar                   %%%%%%%%%%%%%%%%%%%
if isfield(OutputOptionsIn,'Waitbar')
    OutputOptionsOut.Waitbar=OutputOptionsIn.Waitbar;
else
    OutputOptionsOut.Waitbar=1;
end

%%%%%%%%%%%%%%%%            Animate Dynamics            %%%%%%%%%%%%%%%%%%%
% Plot the dynamics in an animation
if isfield(OutputOptionsIn,'PlotAnimation')
    OutputOptionsOut.PlotAnimation=OutputOptionsIn.PlotAnimation;
else
    OutputOptionsOut.PlotAnimation=0;
end

if isfield(OutputOptionsIn,'PlotExactVsFormula')
    OutputOptionsOut.PlotExactVsFormula=OutputOptionsIn.PlotExactVsFormula;
else
    OutputOptionsOut.PlotExactVsFormula=0;
end

% Save the dynamics in an animation
if isfield(OutputOptionsIn,'SaveAnimation')
    OutputOptionsOut.SaveAnimation=OutputOptionsIn.SaveAnimation;
else
    OutputOptionsOut.SaveAnimation=0;
end


%%%%%%%%%%%%%%%%            Plot Results               %%%%%%%%%%%%%%%%%%%%
% Plot the wavepacket
if isfield(OutputOptionsIn,'PlotResults')
    OutputOptionsOut.PlotResults=OutputOptionsIn.PlotResults;
else
    OutputOptionsOut.PlotResults=0;
end

% Plot the error
if isfield(OutputOptionsIn,'PlotError')
    OutputOptionsOut.PlotError=OutputOptionsIn.PlotError;
else
    OutputOptionsOut.PlotError=0;
end

%%%%%%%%%%%%%%%         Perform Checks                %%%%%%%%%%%%%%%%%%%%%
% Total energy
if isfield(OutputOptionsIn,'CheckTotalEnergy')
    OutputOptionsOut.CheckTotalEnergy=OutputOptionsIn.CheckTotalEnergy;
else
    OutputOptionsOut.CheckTotalEnergy=0;
end

% Total mass
if isfield(OutputOptionsIn,'CheckTotalMass')
    OutputOptionsOut.CheckTotalMass=OutputOptionsIn.CheckTotalMass;
else
    OutputOptionsOut.CheckTotalMass=0;
end

% Total momentum
if isfield(OutputOptionsIn,'CheckTotalMomentum')
    OutputOptionsOut.CheckTotalMomentum=OutputOptionsIn.CheckTotalMomentum;
else
    OutputOptionsOut.CheckTotalMomentum=0;
end

% Centre of mass
if isfield(OutputOptionsIn,'CheckCOM')
    OutputOptionsOut.CheckCOM=OutputOptionsIn.CheckCOM;
else
    OutputOptionsOut.CheckCOM=0;
end

% Centre of momentum
if isfield(OutputOptionsIn,'CheckCOMom')
    OutputOptionsOut.CheckCOMom=OutputOptionsIn.CheckCOMom;
else
    OutputOptionsOut.CheckCOMom=0;
end
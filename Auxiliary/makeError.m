function [RelativeError, RelativeAbsError, RelativeMassError] = makeError(psi1,psi2)

    psi1 = psi1(:);
    psi2 = psi2(:);

    % Eigenvectors are only determined up to sign and so are the psi's
    RelP1 = sqrt(sum(abs(psi1 - psi2).^2) ./ sum(abs(psi1).^2));
    RelP2 = sqrt(sum(abs(psi1 - psi2).^2) ./ sum(abs(psi2).^2));
    maxRelP = max(RelP1,RelP2);

    RelM1 = sqrt(sum(abs(psi1 + psi2).^2) ./ sum(abs(psi1).^2));
    RelM2 = sqrt(sum(abs(psi1 + psi2).^2) ./ sum(abs(psi2).^2));
    maxRelM = max(RelM1,RelM2);

    RelativeError = min(maxRelP,maxRelM);
    
    RelAbs1 = sqrt(sum(abs(abs(psi1) - abs(psi2)).^2) ./ sum(abs(psi1).^2));
    RelAbs2 = sqrt(sum(abs(abs(psi1) - abs(psi2)).^2) ./ sum(abs(psi2).^2));
    
    RelativeAbsError = max(RelAbs1,RelAbs2);

    RelMass1 = sqrt(sum(abs(psi1).^2) ./ sum(abs(psi2).^2));
    RelMass2 = sqrt(sum(abs(psi2).^2) ./ sum(abs(psi1).^2));
    
    RelativeMassError = max(RelMass1,RelMass2) - 1;
    
end
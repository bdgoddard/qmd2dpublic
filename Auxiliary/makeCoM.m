function CoM = makeCoM(psi,xg,dx,d)
    Numerics = struct;
    Numerics.d = d;
    Numerics.NumPoints = length(xg);
    repGrid = replicateGrid(xg,Numerics);
    M=prod(dx)*sum(abs(psi(:)).^2);
    CoM = zeros(d,1);
    for dim=1:d
        integrand = abs(psi).^2.*repGrid{dim};
        CoM(dim)  = 1/M.*prod(dx)*sum(integrand(:));
    end

end
function xgRot = RotateGrid2D(xg,theta,center)
    % 2D rotation of grid by angle theta around a point center
    xgRot = zeros(size(xg));

    x = xg(:,:,1);
    x = x - center(1);

    y = xg(:,:,2);
    y = y - center(2);

    xgRot(:,:,1) = cos(theta) * x - sin(theta) * y;
    xgRot(:,:,2) = sin(theta) * x + cos(theta) * y;

    xgRot(:,:,1) = xgRot(:,:,1) + center(1);
    xgRot(:,:,2) = xgRot(:,:,2) + center(2);
end
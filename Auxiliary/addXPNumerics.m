function NumericsOut = addXPNumerics(Numerics)
% Adds x,p,xg,pg to Numerics struct.
    NumericsOut = Numerics;
    [x,p,xg,pg] = makeXP(Numerics);
    NumericsOut.x = x;
    NumericsOut.p = p;
    NumericsOut.xg = xg;
    NumericsOut.pg = pg;

end
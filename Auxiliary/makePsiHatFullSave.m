function psiHat = makePsiHatFullSave(WN,~)

    
    Wavepackets = WN.Wavepackets;
    Numerics    = WN.Numerics;

    psiHatString = Wavepackets.PsiHat;
    
    [~,~,~,pg] = makeXP(Numerics);
    psiHatFn = str2func(psiHatString);
    repGrid = replicateGrid(pg,Numerics);

    psiHat = psiHatFn(Wavepackets,repGrid{:});

end
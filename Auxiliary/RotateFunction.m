function psiRot = RotateFunction(psi,x,theta,center)
    % Rotates a grid with angle theta around center and interpolates function 

    [yg,xg] = meshgrid(x(:,1),x(:,2));  % due to meshgrid inversion
    xg2 = cat(3,xg,yg);
    xgRot = RotateGrid2D(xg2,theta,center);

    % Interpolate onto rotated grid
    psiRot = interp2(yg,xg,psi,xgRot(:,:,2),xgRot(:,:,1),'cubic',0);    
end
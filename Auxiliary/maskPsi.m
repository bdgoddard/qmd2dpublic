function [xgMask,ygMask,psiMask,Mask] = maskPsi(psi,x,xStep,cutOff)

Mask = (abs(psi)>max(abs(psi(:))*cutOff));

[yg,xg] = meshgrid(x(:,1),x(:,2)); % due to meshgrid nonsense

Pos = 1:length(x(:,1));

xMin = min(xg(Mask));
xMax = max(xg(Mask));
xMinLoc = Pos(x(:,1)==xMin);
xMaxLoc = Pos(x(:,1)==xMax);

yMin = min(yg(Mask));
yMax = max(yg(Mask));
yMinLoc = Pos(x(:,2)==yMin);
yMaxLoc = Pos(x(:,2)==yMax);

xMask = x(xMinLoc:xMaxLoc,1);
yMask = x(yMinLoc:yMaxLoc,2);

[ygMask,xgMask] = meshgrid(xMask,yMask); % switched due to meshgrid nonsense

Mask = zeros(size(psi));
Mask(xMinLoc:xMaxLoc,yMinLoc:yMaxLoc) = 1;
Mask = logical(Mask);
psiMask = psi(xMinLoc:xMaxLoc,yMinLoc:yMaxLoc);
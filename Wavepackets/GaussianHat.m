function psihatup = GaussianHat(Wavepackets,varargin)
% Defines a 2D Gaussian wavepacket of form (6.1).

    epsilon = Wavepackets.epsilon;
        
    if(isfield(Wavepackets,'Parameters'))
        Parameters = Wavepackets.Parameters;
    else
        Parameters = struct;
    end
    
    if(isfield(Parameters,'p10'))
        p10 = Parameters.p10;
    else
        p10 = 5;
    end
    
    if(isfield(Parameters,'p20'))
        p20 = Parameters.p20;
    else
        p20 = 0;
    end
    
    if(isfield(Parameters,'x10'))
        x10 = Parameters.x10;
    else
        x10 = 0;
    end
    
    if(isfield(Parameters,'x20'))
        x20 = Parameters.x20;
    else
        x20 = 0;
    end

    p1 = varargin{1};
    
    if nargin == 3
        p2 = varargin{2};
    else
        p2 = 0;
    end
    
    psihatup = exp((-( p1 - p10).^2 - (p2-p20).^2)/(2*epsilon)) ...
                .* exp(-1i*(p1-p10)*x10/epsilon) .* exp(-1i*(p2-p20)*x20/epsilon);
            
    % Normalize        
    dp = p1(2) - p1(1);
    d = nargin - 1;
    N = sqrt(dp^d*sum(abs(psihatup(:)).^2));    
    psihatup = psihatup/N;
end

